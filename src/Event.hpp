#ifndef EVENT_HPP
#define EVENT_HPP

#include <string>
#include <memory>
#include <optional>
#include <sys/types.h>

enum EventType {
    KeepAlive,
    LoginAttempt,
    LoginFailed,
    LoginSucceeded,
    Logout,
    UserSentMessage,
    GroupReceivedMessage,
    UserJoinedGroup,
    UserLeftGroup,
    ElectionStarted,
    ElectionEnded
};

class Event {
  public:
    virtual EventType GetType() = 0;
    int GetClientSocketDescriptor();
    void SetClientSocketDescriptor(int clientSocketDescriptor);
    const std::string& GetMediatorAddress();
    void SetMediatorAddress(const std::string& mediatorAddress);
    int GetMediatorPort();
    void SetMediatorPort(int mediatorPort);
    const std::string& GetTimestamp();
    void SetCurrentTimestamp();
    void SetTimestamp(const std::string& timestamp);
    virtual int GetMarshalledSize() = 0;
    virtual std::shared_ptr<void> Marshal() = 0;
    static std::optional<std::shared_ptr<Event>> Unmarshal(EventType eventType, void* payload);
    static const int FAILURE_REASON_LENGTH{100};
    static const int USER_NAME_LENGTH{20};
    static const int GROUP_NAME_LENGTH{20};
    static const int SERVER_ADDRESS_LENGTH{16};
    static const int MESSAGE_CONTENT_LENGTH{140};
    static const int TIMESTAMP_LENGTH{20};

  protected:
    std::string timestamp;
    int clientSocketDescriptor{-1};
    std::string mediatorAddress;
    int mediatorPort{-1};
};

struct KeepAliveStruct {
    int clientSocketDescriptor;
    char mediatorAddress[Event::SERVER_ADDRESS_LENGTH + 1];
    int mediatorPort;
};

class KeepAliveEvent : public Event {
  public:
    KeepAliveEvent(
        int clientSocketDescriptor = -1,
        const std::string& mediatorAddress = "",
        int mediatorPort = -1);
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<KeepAliveEvent> Unmarshal(KeepAliveStruct* payload);
};

struct LoginAttemptStruct {
    char userName[Event::USER_NAME_LENGTH + 1];
    char groupName[Event::GROUP_NAME_LENGTH + 1];
    int clientSocketDescriptor;
    char mediatorAddress[Event::SERVER_ADDRESS_LENGTH + 1];
    int mediatorPort;
};

class LoginAttemptEvent : public Event {
  public:
    LoginAttemptEvent(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor = -1,
        const std::string& mediatorAddress = "",
        int mediatorPort = -1);
    const std::string& GetUserName();
    const std::string& GetGroupName();
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<LoginAttemptEvent> Unmarshal(LoginAttemptStruct* payload);

  private:
    std::string userName;
    std::string groupName;
};

struct LoginFailedStruct {
    char reason[Event::FAILURE_REASON_LENGTH + 1];
    int clientSocketDescriptor;
};

class LoginFailedEvent : public Event {
  public:
    LoginFailedEvent(const std::string& reason, int clientSocketDescriptor = -1);
    const std::string& GetReason();
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<LoginFailedEvent> Unmarshal(LoginFailedStruct* payload);

  private:
    std::string reason;
};

struct LoginSucceededStruct {
    int clientSocketDescriptor;
};

class LoginSucceededEvent : public Event {
  public:
    LoginSucceededEvent(int clientSocketDescriptor = -1);
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<LoginSucceededEvent> Unmarshal(LoginSucceededStruct* payload);
};

struct LogoutStruct {
    char userName[Event::USER_NAME_LENGTH + 1];
    char groupName[Event::GROUP_NAME_LENGTH + 1];
    int clientSocketDescriptor;
    char mediatorAddress[Event::SERVER_ADDRESS_LENGTH + 1];
    int mediatorPort;
};

class LogoutEvent : public Event {
  public:
    LogoutEvent(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor = -1,
        const std::string& mediatorAddress = "",
        int mediatorPort = -1);
    const std::string& GetUserName();
    const std::string& GetGroupName();
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<LogoutEvent> Unmarshal(LogoutStruct* payload);

  private:
    std::string userName;
    std::string groupName;
};

struct UserSentMessageStruct {
    char messageContent[Event::MESSAGE_CONTENT_LENGTH + 1];
    char userName[Event::USER_NAME_LENGTH + 1];
    char groupName[Event::GROUP_NAME_LENGTH+ 1];
    int clientSocketDescriptor;
    char mediatorAddress[Event::SERVER_ADDRESS_LENGTH + 1];
    int mediatorPort;
};

class UserSentMessageEvent : public Event {
  public:
    UserSentMessageEvent(
        const std::string& userName,
        const std::string& groupName,
        const std::string& messageContent,
        int clientSocketDescriptor = -1,
        const std::string& mediatorAddress = "",
        int mediatorPort = -1);
    const std::string& GetMessageContent();
    const std::string& GetUserName();
    const std::string& GetGroupName();
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<UserSentMessageEvent> Unmarshal(UserSentMessageStruct* payload);

  private:
    std::string messageContent;
    std::string userName;
    std::string groupName;
};

struct GroupReceivedMessageStruct {
    char userName[Event::USER_NAME_LENGTH + 1];
    char groupName[Event::GROUP_NAME_LENGTH + 1];
    char messageContent[Event::MESSAGE_CONTENT_LENGTH + 1];
    char timestamp[Event::TIMESTAMP_LENGTH + 1];
    int clientSocketDescriptor;
};

class GroupReceivedMessageEvent : public Event {
  public:
    GroupReceivedMessageEvent(
        const std::string& userName,
        const std::string& groupName,
        const std::string& messageContent,
        int clientSocketDescriptor = -1);
    GroupReceivedMessageEvent(
        const std::string& userName,
        const std::string& groupName,
        const std::string& messageContent,
        const std::string& timestamp,
        int clientSocketDescriptor = -1);
    const std::string& GetUserName();
    const std::string& GetGroupName();
    const std::string& GetMessageContent();
    const std::string& GetTimestamp();
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<GroupReceivedMessageEvent> Unmarshal(GroupReceivedMessageStruct* payload);

  private:
    std::string userName;
    std::string groupName;
    std::string messageContent;
};

struct UserJoinedGroupStruct {
    char userName[Event::USER_NAME_LENGTH + 1];
    char groupName[Event::GROUP_NAME_LENGTH + 1];
    char timestamp[Event::TIMESTAMP_LENGTH + 1];
    int clientSocketDescriptor;
};

class UserJoinedGroupEvent : public Event {
  public:
    UserJoinedGroupEvent(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor = -1);
    UserJoinedGroupEvent(
        const std::string& userName,
        const std::string& groupName,
        const std::string& timestamp,
        int clientSocketDescriptor = -1);
    const std::string& GetUserName();
    const std::string& GetGroupName();
    const std::string& GetTimestamp();
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<UserJoinedGroupEvent> Unmarshal(UserJoinedGroupStruct* payload);

  private:
    std::string userName;
    std::string groupName;
};

struct UserLeftGroupStruct {
    char userName[Event::USER_NAME_LENGTH + 1];
    char groupName[Event::GROUP_NAME_LENGTH + 1];
    char timestamp[Event::TIMESTAMP_LENGTH + 1];
    int clientSocketDescriptor;
};

class UserLeftGroupEvent : public Event {
  public:
    UserLeftGroupEvent(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor = -1);
    UserLeftGroupEvent(
        const std::string& userName,
        const std::string& groupName,
        const std::string& timestamp,
        int clientSocketDescriptor = -1);
    const std::string& GetUserName();
    const std::string& GetGroupName();
    const std::string& GetTimestamp();
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<UserLeftGroupEvent> Unmarshal(UserLeftGroupStruct* payload);

  private:
    std::string userName;
    std::string groupName;
};

struct ElectionStartedStruct {
    char candidateAddress[Event::SERVER_ADDRESS_LENGTH + 1];
    int candidatePort;
};

class ElectionStartedEvent : public Event {
  public:
    ElectionStartedEvent(const std::string& candidateAddress, int candidatePort);
    const std::string& GetCandidateAddress();
    void SetCandidateAddress(const std::string& candidateAddress);
    int GetCandidatePort();
    void SetCandidatePort(int candidatePort);
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<ElectionStartedEvent> Unmarshal(ElectionStartedStruct* payload);

  private:
    std::string candidateAddress;
    int candidatePort;
};

struct ElectionEndedStruct {
    char principalAddress[Event::SERVER_ADDRESS_LENGTH + 1];
    int principalPort;
};

class ElectionEndedEvent : public Event {
  public:
    ElectionEndedEvent(const std::string& perincipalAddress, int principalPort);
    const std::string& GetPrincipalAddress();
    void SetPrincipalAddress(const std::string& principalAddress);
    int GetPrincipalPort();
    void SetPrincipalPort(int principalPort);
    EventType GetType() override;
    int GetMarshalledSize() override;
    std::shared_ptr<void> Marshal() override;
    static std::shared_ptr<ElectionEndedEvent> Unmarshal(ElectionEndedStruct* payload);

  private:
    std::string principalAddress;
    int principalPort;
};

#endif
