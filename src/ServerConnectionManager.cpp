# include <unistd.h>
#include <cstring>
#include <algorithm>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <exception>
#include "../lib/csv.hpp"
#include "ServerConnectionManager.hpp"
#include "ServerDispatcher.hpp"

using namespace std;
using namespace csv;

ServerConnectionManager ServerConnectionManager::instance;

ServerConnectionManager& ServerConnectionManager::GetInstance() {
    // singleton pattern
    return instance;
}

void ServerConnectionManager::Init() {
    // Read servers configuration
    CSVReader serversReader("./config/servers_config.csv");
    for (const auto& row : serversReader) {
        logger.Debug("Loading a server!");

        string address(row["address"].get<>());
        int port(stoi(row["port"].get<>()));

        backEndServers.insert(make_pair(make_pair(address, port), -1));
    }
    // Read servers configuration
    CSVReader mediatorsReader("./config/mediators_config.csv");
    for (const auto& row : mediatorsReader) {
        logger.Debug("Loading a mediator!");

        string address(row["address"].get<>());
        int port(stoi(row["port"].get<>()));

        frontEndServers.insert(make_pair(make_pair(address, port), -1));
    }
}

void ServerConnectionManager::StartWaitingForConnections(int serverPort) {
    new thread(&ServerConnectionManager::WaitForConnections, this, serverPort);
}

void ServerConnectionManager::WaitForConnections(int serverPort) {
    // create the server's socket
    int serverSocket{socket(AF_INET, SOCK_STREAM, 0)};
	if (serverSocket == -1) {
		logger.Error("Error creating the server's socket: " + string(strerror(errno)));
        throw runtime_error("socket(): " + string(strerror(errno)));
	}

	// setup remote address
	sockaddr_in serverSocketAddress{
		.sin_family{AF_INET},
		.sin_port{htons(serverPort)},
		.sin_addr{
            .s_addr{INADDR_ANY},
        },
        .sin_zero{0},
	};

    // bind socket
	int bound{bind(serverSocket, (sockaddr *) &serverSocketAddress, sizeof(serverSocketAddress))};
	if (bound == -1) {
        logger.Error("Error on binding: " + string(strerror(errno)));
        throw runtime_error("bind(): " + string(strerror(errno)));
	}

    // listen to socket
    int listened{listen(serverSocket, MAX_SIMULTANEOUS_SOCKET_CLIENTS)};
	if (listened == -1) {
        logger.Error("Error on listening: " + string(strerror(errno)));
        throw runtime_error("listen(): " + string(strerror(errno)));
	}

    logger.Info("Server listening on port " + to_string(serverPort));

    // process incoming connections
    while (true) {
        sockaddr_in peerSocketAddress{0};
        socklen_t peerSocketAddressLength{0};
        int peerSocket{accept(serverSocket, (sockaddr *) &peerSocketAddress, &peerSocketAddressLength)};
        if (peerSocket == -1) {
            logger.Error("Error on accepting peer: " + string(strerror(errno)));
            continue;
        }

        auto peerAddress{string(inet_ntoa(peerSocketAddress.sin_addr))};
        auto peerPort{ntohs(peerSocketAddress.sin_port)};
        logger.Info("Established connection with peer " + peerAddress + ":" + to_string(peerPort));

        // create mutex for writing to this socket
        socketDescriptorMutexes[peerSocket];

        // create thread for reading from this socket
        new thread(&ServerConnectionManager::ListenForEvents, this, peerSocket);
    }
}

void ServerConnectionManager::ConnectToMediatorsAsPrincipal() {
    auto electionEndedEvent{make_unique<ElectionEndedEvent>(serverAddress, serverPort)};

    for (auto& frontEndServer : frontEndServers) {
        auto frontEndAddress = frontEndServer.first.first;
        auto frontEndPort = frontEndServer.first.second;

        try {
            // connect to front-end and store its socket descriptor
            int mediatorSocketDescriptor = Connect(frontEndAddress, frontEndPort);
            frontEndServer.second = mediatorSocketDescriptor;

            // notify it we've won the election and start listening for the events it'll send
            Send(mediatorSocketDescriptor, electionEndedEvent.get());
            logger.Info("Presented as principal to front-end server " +
                frontEndAddress + ":" + to_string(frontEndPort));
            new thread(&ServerConnectionManager::ListenForEvents, this, mediatorSocketDescriptor);
        }
        catch (...) {
            logger.Debug("Front-end server " + frontEndAddress + ":" +
                to_string(frontEndPort) + " not available");
        }
    }
}

int ServerConnectionManager::GetMediatorSocketDescriptorByAddressAndPort(
        const std::string& mediatorAddress,
        int mediatorPort) {
    return frontEndServers.at(make_pair(mediatorAddress, mediatorPort));
}

void ServerConnectionManager::Send(int socketDescriptor, Event* event) {
    // lock this socket descriptor to prevent simultaneous writes
    scoped_lock lock(socketDescriptorMutexes[socketDescriptor]);

	ConnectionManager::Send(socketDescriptor, event);
}

void ServerConnectionManager::ForwardToPeers(Event* event) {
    // TODO: keeo these connections open?
    for (const auto& backEndServer : backEndServers) {
        auto backEndAddress = backEndServer.first.first;
        auto backEndPort = backEndServer.first.second;

        // don't forward to itself
        if (backEndAddress == serverAddress && backEndPort == serverPort) {
            continue;
        }

        try {
            int peerSocketDescriptor = Connect(backEndAddress, backEndPort);
            Send(peerSocketDescriptor, event);
            logger.Info("Forwarded event to peer " + backEndAddress + ":" + to_string(backEndPort));
        }
        catch (...) {
            logger.Debug("Peer " + backEndAddress + ":" + to_string(backEndPort) + " not available");
        }
    }
}

void ServerConnectionManager::ListenForEvents(int socketDescriptor) {
    auto& dispatcher{ServerDispatcher::GetInstance()};

    while (true) {
        try {
            // read and dispatch event
            auto event{Receive(socketDescriptor)};
            dispatcher.Dispatch(event.get());
        }
        catch (exception& e) {
            logger.Error("An error occurred: " + string(e.what()));
            break;
        }
    }
}

void ServerConnectionManager::SetMaxMessagesToSend(unsigned long maxMessagesToSend) {
    this->maxMessagesToSend = maxMessagesToSend;
}

unsigned long ServerConnectionManager::GetMaxMessagesToSend() {
    return maxMessagesToSend;
}

int ServerConnectionManager::Connect(const string& serverAddress, int serverPort) {
    // create socket
    int socketDescriptor = -1;
	socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (socketDescriptor == -1) {
		logger.Error("Error creating the new socket: " + string(strerror(errno)));
		throw runtime_error("socket(): " + string(strerror(errno)));
	}

	// setup remote address
	sockaddr_in socketAddress{
		.sin_family{AF_INET},
		.sin_port{htons(serverPort)},
		.sin_addr{
			.s_addr{inet_addr(serverAddress.c_str())},
		},
        .sin_zero{0},
	};

	// initiate socket connection
	int connected{connect(socketDescriptor, (sockaddr*) &socketAddress, sizeof(socketAddress))};
	if (connected == -1) {
        logger.Error("Error connecting: " + string(strerror(errno)));
		throw runtime_error("connect(): " + string(strerror(errno)));
	}

	logger.Debug("Connected to " + serverAddress + ":" + to_string(serverPort));

    return socketDescriptor;
}

int ServerConnectionManager::ConnectToRingPeer() {
    auto itself = backEndServers.find(pair<string, int>(serverAddress, serverPort));

    // try to find the next available peer in the logical ring
    auto it = itself;
    while (true) {
        it++;

        // if end, goes back to start
        if (it == backEndServers.end()) {
            it = backEndServers.begin();
        }

        // if looped around, give up, as we're the only back-end server available
        if (it == itself) {
            throw runtime_error("No peer responded");
        }

        // attempt to connect to the next server in the ring
        auto backEndAddress = it->first.first;
        auto backEndPort = it->first.second;
        try {
            logger.Debug("Attempting to connect to peer " + backEndAddress + ":" + to_string(backEndPort));
            int peerSocketDescriptor = Connect(backEndAddress, backEndPort);

            logger.Info("Connected to peer " + backEndAddress + ":" + to_string(backEndPort));
            return peerSocketDescriptor;
        }
        catch (...) {
            logger.Debug("Peer " + backEndAddress + ":" + to_string(backEndPort) + " not available");
        }
    }
}

void ServerConnectionManager::StartElection() {
    logger.Info("Starting an election");

    auto& dispatcher{ServerDispatcher::GetInstance()};
    auto lock = dispatcher.LockElectionState();
    dispatcher.SetElectedAsPrincipal(false);
    dispatcher.SetElectionOngoing(true);

    try {
        int peerSocketDescriptor = ConnectToRingPeer();
	    auto event{make_unique<ElectionStartedEvent>(serverAddress, serverPort)};
        Send(peerSocketDescriptor, event.get());
    }
    catch(...) {
        logger.Info("Failed to connect to a ring peer");
        logger.Info("Setting itself as principal");
        dispatcher.SetElectionOngoing(false);
        dispatcher.SetElectedAsPrincipal(true);
        ConnectToMediatorsAsPrincipal();
    }
}

void ServerConnectionManager::StartMonitoringPrincipal(const string& principalAddress, int principalPort) {
    new thread(&ServerConnectionManager::MonitorPrincipal, this, principalAddress, principalPort);
}

void ServerConnectionManager::MonitorPrincipal(const string& principalAddress, int principalPort) {
    auto& dispatcher{ServerDispatcher::GetInstance()};

    while (true) {
        // stop monitoring if the principal is no longer there
        if (dispatcher.GetElectionOngoing() || dispatcher.GetElectedAsPrincipal()) {
            return;
        }

        // attempt to connect to the principal
        try {
            logger.Debug("Attempting to connect to principal " + principalAddress + ":" + to_string(principalPort));
            int principalSocketDescriptor = Connect(principalAddress, principalPort);
            close(principalSocketDescriptor);

            // if connection succeeds, wait for a few seconds and try again
            logger.Debug("Connected to principal " + principalAddress + ":" + to_string(principalPort));
        }
        catch (...) {
            // if connection fails, start an election
            logger.Info("Principal " + principalAddress + ":" + to_string(principalPort) + " not available");
            StartElection();
        }

        sleep(5);
    }
}

const string& ServerConnectionManager::GetServerAddress() {
    return serverAddress;
}

void ServerConnectionManager::SetServerAddress(const string& serverAddress) {
    this->serverAddress = serverAddress;
}

int ServerConnectionManager::GetServerPort() {
    return serverPort;
}

void ServerConnectionManager::SetServerPort(int serverPort) {
    this->serverPort = serverPort;
}
