#ifndef SESSION_MANAGER_HPP
#define SESSION_MANAGER_HPP

#include <string>
#include <vector>
#include <mutex>
#include <memory>
#include <optional>
#include <unordered_map>
#include "Session.hpp"
#include "Event.hpp"
#include "LoggingHelper.hpp"

class SessionManager {
  public:
    static const int MAX_CONNECTIONS_PER_USER{2};

    static SessionManager& GetInstance();
    std::vector<std::shared_ptr<Session>> GetUserSessions(const std::string& userName);
    std::vector<std::shared_ptr<Session>> GetUserSessionsForGroup(
        const std::string& userName,
        const std::string& groupName);
    std::vector<std::shared_ptr<Session>> GetAllSessions();
    std::optional<std::shared_ptr<Session>> GetSessionByIdentifiers(
        int clientSocketDescriptor,
        const std::string& mediatorAddress,
        int mediatorPort);
    void RemoveUserSessionByIdentifiers(
        const std::string& userName,
        int clientSocketDescriptor,
        const std::string& mediatorAddress,
        int mediatorPort);
    void HandleLoginAttempt(LoginAttemptEvent* event);
    void HandleLogout(LogoutEvent* event);
    void CreateNewSession(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor,
        const std::string& mediatorAddress,
        int mediatorPort);
    void StartCheckingSessions();
    void CheckSessions();

  private:
    SessionManager() {};
    SessionManager(SessionManager const &) = delete;
    SessionManager &operator=(SessionManager const &) = delete;

    static SessionManager instance;
    LoggingHelper logger{INFO};
    std::unordered_map<std::string, std::vector<std::shared_ptr<Session>>> userSessions;
    std::mutex userSessionsMutex;
    bool CanUserLogIn(const std::string& userName);
};

#endif
