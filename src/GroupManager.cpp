#include <regex>
#include "GroupManager.hpp"
#include "SessionManager.hpp"
#include "ServerConnectionManager.hpp"
#include "ServerDispatcher.hpp"

using namespace std;

GroupManager GroupManager::instance;

GroupManager& GroupManager::GetInstance() {
    // singleton pattern
    return instance;
}

optional<shared_ptr<Group>> GroupManager::GetGroup(const string& groupName) {
    scoped_lock lock(groupsMutex);

    auto it{groups.find(groupName)};

    if (it == groups.end()) {
        return {};
    }

    return it->second;
}

shared_ptr<Group> GroupManager::GetOrCreateGroup(const string& groupName) {
    auto group{GetGroup(groupName)};

    if (!group) {
        logger.Info("Creating group " + groupName);
        auto newGroup{make_shared<Group>(groupName)};
        AddGroup(newGroup);
        group = newGroup;
    }

    return group.value();
}

void GroupManager::AddUserToGroup(
        const string& userName,
        const string& groupName,
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    auto group{GetOrCreateGroup(groupName)};
    group->AddUser(userName);

    auto& dispatcher{ServerDispatcher::GetInstance()};
    auto userJoinedGroupEvent{make_unique<UserJoinedGroupEvent>(userName, groupName)};
    userJoinedGroupEvent->SetClientSocketDescriptor(clientSocketDescriptor);
    userJoinedGroupEvent->SetMediatorAddress(mediatorAddress);
    userJoinedGroupEvent->SetMediatorPort(mediatorPort);
    dispatcher.Dispatch(userJoinedGroupEvent.get());
}

void GroupManager::RemoveUserFromGroup(
        const string& userName,
        const string& groupName,
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    auto group{GetOrCreateGroup(groupName)};
    group->RemoveUser(userName);

    auto& disptacher{ServerDispatcher::GetInstance()};
    auto userLeftGroupEvent{make_unique<UserLeftGroupEvent>(userName, groupName)};
    userLeftGroupEvent->SetClientSocketDescriptor(clientSocketDescriptor);
    userLeftGroupEvent->SetMediatorAddress(mediatorAddress);
    userLeftGroupEvent->SetMediatorPort(mediatorPort);
    disptacher.Dispatch(userLeftGroupEvent.get());
}

bool GroupManager::IsGroupNameValid(const string& groupName) {
    // groups must have between 4 and 20 characters
    // first character must be a letter
    // the rest can be either a letter, a number or a dot (.)
    regex validGroupNameRegex("^[a-zA-Z][a-zA-Z0-9.]{3," + to_string(Event::GROUP_NAME_LENGTH - 1) + "}$");
    return regex_match(groupName, validGroupNameRegex);
}

void GroupManager::AddGroup(const shared_ptr<Group>& group) {
    scoped_lock lock(groupsMutex);

    pair<string, const shared_ptr<Group>&> groupPair(group->GetGroupName(), group);
    groups.insert(groupPair);
}

bool GroupManager::GroupExists(const string& groupName) {
    scoped_lock lock(groupsMutex);

    auto it{groups.find(groupName)};
    return it != groups.end();
}

void GroupManager::BroadcastMessage(const string& groupName, Event* event) {
    auto& serverDispatcher{ServerDispatcher::GetInstance()};
    if (!serverDispatcher.GetElectedAsPrincipal()) {
        logger.Info("Would have broadcast event to group " + groupName);
        return;
    }

    logger.Info("Broadcasting event to group " + groupName);

    auto& sessionManager{SessionManager::GetInstance()};
    auto& connectionManager{ServerConnectionManager::GetInstance()};

    auto group{GetGroup(groupName)};
    if (group) {
        auto groupUsers{group.value()->GetAllUsers()};
        for (const auto& user : groupUsers) {
            logger.Debug("Looping through user " + user + "'s sessions");

            auto userSessions{sessionManager.GetUserSessions(user)};
            for (const auto& session : userSessions) {
                if (session->GetGroupName() == groupName) {
                    event->SetClientSocketDescriptor(session->GetClientSocketDescriptor());
                    event->SetMediatorAddress(session->GetMediatorAddress());
                    event->SetMediatorPort(session->GetMediatorPort());
                    int mediatorSocketDescriptor = connectionManager.GetMediatorSocketDescriptorByAddressAndPort(
                        session->GetMediatorAddress(),
                        session->GetMediatorPort()
                    );
                    connectionManager.Send(mediatorSocketDescriptor, event);
                }
            }
        }
    }
}
