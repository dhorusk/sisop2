#include <thread>
#include <algorithm>
#include <unistd.h>
#include "SessionManager.hpp"
#include "UserManager.hpp"
#include "GroupManager.hpp"
#include "ServerDispatcher.hpp"

using namespace std;

SessionManager SessionManager::instance;

SessionManager& SessionManager::GetInstance() {
    // singleton pattern
    return instance;
}

vector<shared_ptr<Session>> SessionManager::GetUserSessions(const string& userName) {
    // lock userSessions
    scoped_lock lock(userSessionsMutex);

    auto it{userSessions.find(userName)};

    // return empty list when user has no connections
    if (it == userSessions.end()) {
        return vector<shared_ptr<Session>>();
    }

    // return a copy of the vector if the user has connections
    return it->second;
}

vector<shared_ptr<Session>> SessionManager::GetUserSessionsForGroup(
        const string& userName,
        const string& groupName) {
    // lock userSessions
    scoped_lock lock(userSessionsMutex);

    auto it{userSessions.find(userName)};

    // return empty list when user has no connections
    if (it == userSessions.end()) {
        return vector<shared_ptr<Session>>();
    }

    auto sessions{vector<shared_ptr<Session>>()};

    // return the user's sessions for this group
    for (const auto& session : it->second) {
        if (session->GetUserName() == groupName) {
            sessions.push_back(session);
        }
    }

    // return a filtered copy of the vector
    return sessions;
}

vector<shared_ptr<Session>> SessionManager::GetAllSessions() {
    // lock userSessions
    scoped_lock lock(userSessionsMutex);

    // return a copy of all the connections in a flattened list
    auto allConnections{vector<shared_ptr<Session>>()};

    // for each user
    for (const auto& userConnections : userSessions) {
        // insert all the connections of that single user
        allConnections.insert(
            allConnections.cend(),
            userConnections.second.cbegin(),
            userConnections.second.cend()
        );
    }

    return allConnections;
}

optional<shared_ptr<Session>> SessionManager::GetSessionByIdentifiers(
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    // lock userSessions
    scoped_lock lock(userSessionsMutex);

    for (const auto& it : userSessions) {
        for (const auto& session : it.second) {
            if (clientSocketDescriptor == session->GetClientSocketDescriptor()
                    && mediatorAddress == session->GetMediatorAddress()
                    && mediatorPort == session->GetMediatorPort()) {
                return session;
            }
        }
    }

    return {};
}

void SessionManager::RemoveUserSessionByIdentifiers(
        const string& userName,
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    // lock userSessions
    scoped_lock lock(userSessionsMutex);

    auto it{userSessions.find(userName)};
    if (it == userSessions.end()) {
        logger.Error("Attempting to remove inexistent session for user " + userName);
        return;
    }

    auto& sessions{it->second};
    sessions.erase(
        remove_if(
            sessions.begin(),
            sessions.end(),
            [clientSocketDescriptor, mediatorAddress, mediatorPort](auto const& session) {
                return (session->GetClientSocketDescriptor() == clientSocketDescriptor
                    && session->GetMediatorAddress() == mediatorAddress
                    && session->GetMediatorPort() == mediatorPort);
            }
        ),
        sessions.end());
}

bool SessionManager::CanUserLogIn(const string& userName) {
    // lock userSessions
    scoped_lock lock(userSessionsMutex);

    auto it{userSessions.find(userName)};

    // user can connect when they have no connections
    if (it == userSessions.end()) {
        return true;
    }

    // or if the limit of simultaneous connections wasn't reached
    if (it->second.size() < MAX_CONNECTIONS_PER_USER) {
        return true;
    }

    return false;
}

void SessionManager::CreateNewSession(
        const string& userName,
        const string& groupName,
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    auto userCurrentSessions{GetUserSessions(userName)};

    // lock userSessions
    scoped_lock lock(userSessionsMutex);

    auto newSession{make_shared<Session>(
        userName,
        groupName,
        clientSocketDescriptor,
        mediatorAddress,
        mediatorPort)};

    if (userCurrentSessions.empty()) {
        vector<shared_ptr<Session>> sessions;
        pair<string, vector<shared_ptr<Session>>> sessionPair(userName, sessions);
        userSessions.insert(sessionPair);
    }
    auto it{userSessions.find(userName)};
    it->second.push_back(newSession);

    // notify client
    auto& serverDispatcher{ServerDispatcher::GetInstance()};
    auto loginSucceededEvent{make_unique<LoginSucceededEvent>()};
    loginSucceededEvent->SetClientSocketDescriptor(clientSocketDescriptor);
    loginSucceededEvent->SetMediatorAddress(mediatorAddress);
    loginSucceededEvent->SetMediatorPort(mediatorPort);
    serverDispatcher.Dispatch(loginSucceededEvent.get());
}

void SessionManager::HandleLogout(LogoutEvent* event) {
    // remove the user's session
    RemoveUserSessionByIdentifiers(
        event->GetUserName(),
        event->GetClientSocketDescriptor(),
        event->GetMediatorAddress(),
        event->GetMediatorPort());

    // remove user from group if there is no session left
    auto sessionsInGroup{GetUserSessionsForGroup(event->GetUserName(), event->GetGroupName())};
    if (sessionsInGroup.empty()) {
        auto& groupManager{GroupManager::GetInstance()};
        groupManager.RemoveUserFromGroup(
            event->GetUserName(),
            event->GetGroupName(),
            event->GetClientSocketDescriptor(),
            event->GetMediatorAddress(),
            event->GetMediatorPort());
    }
}

void SessionManager::HandleLoginAttempt(LoginAttemptEvent* event) {
    auto& serverDispatcher{ServerDispatcher::GetInstance()};
    auto& userManager{UserManager::GetInstance()};
    auto& groupManager{GroupManager::GetInstance()};
    auto userName{event->GetUserName()};
    auto groupName{event->GetGroupName()};

    if (!userManager.IsUserNameValid(userName)) {
        logger.Info("Login failed: invalid username");
        auto loginFailedEvent{make_unique<LoginFailedEvent>("Invalid username")};
        loginFailedEvent->SetClientSocketDescriptor(event->GetClientSocketDescriptor());
        loginFailedEvent->SetMediatorAddress(event->GetMediatorAddress());
        loginFailedEvent->SetMediatorPort(event->GetMediatorPort());
        serverDispatcher.Dispatch(loginFailedEvent.get());
        return;
    }

    if (!groupManager.IsGroupNameValid(groupName)) {
        logger.Info("Login failed: invalid group name");
        auto loginFailedEvent{make_unique<LoginFailedEvent>("Invalid group name")};
        loginFailedEvent->SetClientSocketDescriptor(event->GetClientSocketDescriptor());
        loginFailedEvent->SetMediatorAddress(event->GetMediatorAddress());
        loginFailedEvent->SetMediatorPort(event->GetMediatorPort());
        serverDispatcher.Dispatch(loginFailedEvent.get());
        return;
    }

    if (!CanUserLogIn(userName)) {
        logger.Info("Login failed: user exceeded the maximum number of connections");
        auto loginFailedEvent{make_unique<LoginFailedEvent>("User has exceeded the maximum number of connections")};
        loginFailedEvent->SetClientSocketDescriptor(event->GetClientSocketDescriptor());
        loginFailedEvent->SetMediatorAddress(event->GetMediatorAddress());
        loginFailedEvent->SetMediatorPort(event->GetMediatorPort());
        serverDispatcher.Dispatch(loginFailedEvent.get());
        return;
    }
    
    logger.Info("Creating new session for user " + userName + " in group " + groupName +
        " with clientSocketDescriptor " + to_string(event->GetClientSocketDescriptor()) +
        " and mediatorAddress " + event->GetMediatorAddress() + ":" + to_string(event->GetMediatorPort()));

    // Create new session
    CreateNewSession(
        userName,
        groupName,
        event->GetClientSocketDescriptor(),
        event->GetMediatorAddress(),
        event->GetMediatorPort());

    // add user if doesn't exist
    userManager.AddUser(userName);

    // add user to group
    groupManager.AddUserToGroup(
        userName,
        groupName,
        event->GetClientSocketDescriptor(),
        event->GetMediatorAddress(),
        event->GetMediatorPort());
}

void SessionManager::StartCheckingSessions() {
    new thread(&SessionManager::CheckSessions, this);
}

void SessionManager::CheckSessions() {
    unique_lock<mutex> lock(userSessionsMutex, defer_lock);

    auto& dispatcher{ServerDispatcher::GetInstance()};
    while (true) {
        // only check the sessions if we're the principal
        if (dispatcher.GetElectedAsPrincipal()) {
            // lock userSessions
            lock.lock();

            auto allSessions{userSessions};
            for (const auto& it : allSessions) {
                auto allUserSessions{it.second};
                for (const auto& session : allUserSessions) {
                    if (!session->IsSessionActive()) {
                        logger.Info("Session for user " + session->GetUserName() +
                            " in group " + session->GetGroupName() + " closed due to inactivity");

                        // prevent deadlock while removing
                        lock.unlock();
                        RemoveUserSessionByIdentifiers(
                            session->GetUserName(),
                            session->GetClientSocketDescriptor(),
                            session->GetMediatorAddress(),
                            session->GetMediatorPort());
                        lock.lock();
                    }
                }
            }

            // unlock userSessions
            lock.unlock();
        }

        sleep(60);
    }
}
