#include <string>
#include <iostream>
#include <csignal>
#include "UIManager.hpp"
#include "ClientDispatcher.hpp"

using namespace std;

UIManager UIManager::instance;

UIManager& UIManager::GetInstance() {
    // singleton pattern
    return instance;
}

UIManager::~UIManager() {
	delwin(chatWindow);
	delwin(inputWindow);
	delwin(chatWindowFrame);
	delwin(inputWindowFrame);
	endwin();
}

bool UIManager::initialized{false};

void UIManager::Init(const string& userName, const string& groupName) {
	if (initialized) {
		return;
	}

	this->userName = userName;
	this->groupName = groupName;

	initscr();

	chatWindowFrame = newwin(20, 80, 0, 0);
	box(chatWindowFrame, 0, 0);
	string chatLabel(" Group: " + groupName + " ");
	mvwaddstr(chatWindowFrame, 0, 2, chatLabel.c_str());
	wrefresh(chatWindowFrame);

	chatWindow = newwin(20-2, 80-2, 0+1, 0+1);
	scrollok(chatWindow, TRUE);

	inputWindowFrame = newwin(4, 80, 20, 0);
	box(inputWindowFrame, 0, 0);
	string inputLabel(" User: " + userName + " ");
	mvwaddstr(inputWindowFrame, 0, 2, inputLabel.c_str());
	wrefresh(inputWindowFrame);

	inputWindow = newwin(4-2, 80-2, 20+1, 0+1);

	initialized = true;
}

void UIManager::ProcessUserInput() {
    unique_lock<mutex> lock(terminalMutex, defer_lock);
	char messageBuffer[Event::MESSAGE_CONTENT_LENGTH + 1];
	auto& dispatcher{ClientDispatcher::GetInstance()};

	while (true) {
        // lock terminal
        lock.lock();

		// clear previous message
		werase(inputWindow);
		wrefresh(inputWindow);

        // unlock terminal
        lock.unlock();

		// read new message
		mvwgetnstr(inputWindow, 0, 0, messageBuffer, Event::MESSAGE_CONTENT_LENGTH);

		// handle Ctrl+D
		if (messageBuffer[0] == 0x04) {
			raise(SIGINT);
		}

		// send it to the server
		auto event{make_unique<UserSentMessageEvent>(userName, groupName, string(messageBuffer))};
		dispatcher.Dispatch(event.get());
	}
}

void UIManager::DisplayMessage(const string& author, const string& content, const string& timestamp) {
	// lock terminal
    scoped_lock lock(terminalMutex);

	// store cursor
	int y{0};
	int x{0};
	getsyx(y, x);

	string line{"[" + timestamp + "] "};
	line += "<" + (author == userName ? "You" : author) + "> ";
	line += content + "\n";
	waddstr(chatWindow, line.c_str());
	wrefresh(chatWindow);

	// restore cursor
	wnoutrefresh(chatWindow);
	wnoutrefresh(inputWindow);
	setsyx(y, x);
	doupdate();
}

void UIManager::DisplayEvent(const string& eventMessage, const string& timestamp) {
	// lock terminal
    scoped_lock lock(terminalMutex);

	// store cursor
	int y{0};
	int x{0};
	getsyx(y, x);

	string line{"[" + timestamp + "] "};
	line += eventMessage + "\n";
	waddstr(chatWindow, line.c_str());
	wrefresh(chatWindow);

	// restore cursor
	wnoutrefresh(chatWindow);
	wnoutrefresh(inputWindow);
	setsyx(y, x);
	doupdate();
}
