#ifndef MESSAGE_REPOSITORY_HPP
#define MESSAGE_REPOSITORY_HPP

#include <string>
#include <vector>
#include <memory>
#include "Message.hpp"
#include "LoggingHelper.hpp"

class MessageRepository {
  public:
    MessageRepository(const std::string& groupName);
    std::vector<std::shared_ptr<Message>> GetAllMessages();
    void StoreMessage(Message* message);

  private:
    std::string groupName;
    std::string fileName;
    LoggingHelper logger{INFO};
};

#endif
