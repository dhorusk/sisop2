#ifndef CLIENT_DISPATCHER_HPP
#define CLIENT_DISPATCHER_HPP

#include "Event.hpp"
#include "LoggingHelper.hpp"

class ClientDispatcher {
  public:
    static ClientDispatcher& GetInstance();
    void Dispatch(Event* event);
    void Dispatch(LoginFailedEvent* event);
    void Dispatch(LoginSucceededEvent* event);
    void Dispatch(LogoutEvent* event);
    void Dispatch(UserSentMessageEvent* event);
    void Dispatch(GroupReceivedMessageEvent* event);
    void Dispatch(UserJoinedGroupEvent* event);
    void Dispatch(UserLeftGroupEvent* event);
    void Dispatch(KeepAliveEvent* event);

  private:
    ClientDispatcher() {};
    ClientDispatcher(ClientDispatcher const &) = delete;
    ClientDispatcher &operator=(ClientDispatcher const &) = delete;

    static ClientDispatcher instance;
    LoggingHelper logger{ERROR};
};

#endif
