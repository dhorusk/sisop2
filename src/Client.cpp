#include <iostream>
#include <csignal>
#include "ClientConnectionManager.hpp"
#include "LoggingHelper.hpp"
#include "UIManager.hpp"

using namespace std;

void signalHandler(int signum) {
	auto& uiManager{UIManager::GetInstance()};
	uiManager.~UIManager();

	auto& connectionManager{ClientConnectionManager::GetInstance()};
	connectionManager.Logout();

	exit(signum);
}

int main(int argc, char* argv[]) {
	// show help if not all args were given
	if (argc < 5) {
		cout << "Usage:" << endl;
		cout << "\t" << argv[0] << " USER_NAME GROUP_NAME SERVER_ADDRESS SERVER_PORT" << endl;
		cout << endl;
		cout << "Where:" << endl;
		cout << "\t" << "USER_NAME: Name of the user logging in" << endl;
		cout << "\t" << "GROUP_NAME: Name of the group to log in to" << endl;
		cout << "\t" << "MEDIATOR_ADDRESS: Network address of the mediator" << endl;
		cout << "\t" << "MEDIATOR_PORT: Network port of the mediator" << endl;
		exit(EXIT_SUCCESS);
	}

	signal(SIGINT, signalHandler);

	LoggingHelper logger(ERROR);
	string userName{argv[1]};
	string groupName{argv[2]};
	string serverAddress{argv[3]};
	int serverPort{0};

	// try to parse serverPort
	try {
		serverPort = stoi(argv[4]);
		logger.Info("MEDIATOR_PORT set to " + to_string(serverPort));
	}
	catch (...) {
		logger.Error("Invalid value for MEDIATOR_PORT");
		exit(EXIT_FAILURE);
	}

	auto& uiManager{UIManager::GetInstance()};
	uiManager.Init(userName, groupName);

	auto& connectionManager{ClientConnectionManager::GetInstance()};
	connectionManager.LoginToServer(userName, groupName, serverAddress, serverPort);

	uiManager.ProcessUserInput();

	return 0;
}
