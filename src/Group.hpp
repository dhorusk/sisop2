#ifndef GROUP_HPP
#define GROUP_HPP

#include <vector>
#include <unordered_set>
#include <memory>
#include <string>
#include <mutex>
#include "Message.hpp"
#include "MessageRepository.hpp"
#include "LoggingHelper.hpp"

class Group {
  public:
    Group(const std::string& groupName);
    const std::string& GetGroupName();
    const std::unordered_set<std::string>& GetAllUsers();
    void AddUser(const std::string& userName);
    void RemoveUser(const std::string& userName);
    void AddMessage(Message message, bool persistToFile);
    const std::vector<std::shared_ptr<Message>>& GetMessages();
    bool IsUserAlreadyInTheGroup(const std::string& userName);

  private:
    std::unordered_set<std::string> users;
    std::mutex usersMutex;
    std::vector<std::shared_ptr<Message>> messages;
    std::mutex messagesMutex;
    MessageRepository repository;
    std::mutex repositoryMutex;
    std::string name;
    LoggingHelper logger{ERROR};
};

#endif
