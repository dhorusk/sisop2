#include "Session.hpp"

using namespace std;

Session::Session(
		const string& userName,
		const string& groupName,
		int clientSocketDescriptor,
		const string& mediatorAddress,
		int mediatorPort) {
	this->userName = userName;
	this->groupName = groupName;
	this->clientSocketDescriptor = clientSocketDescriptor;
	this->mediatorAddress = mediatorAddress;
	this->mediatorPort = mediatorPort;
}

const string& Session::GetGroupName() {
	return groupName;
}

const string& Session::GetUserName() {
	return userName;
}

int Session::GetClientSocketDescriptor() {
	return clientSocketDescriptor;
}

const string& Session::GetMediatorAddress() {
	return mediatorAddress;
}

int Session::GetMediatorPort() {
	return mediatorPort;
}

void Session::UpdateHeartbeat() {
	lastHeartbeatTimestamp = time(0);
}

bool Session::IsSessionActive() {
	auto now{time(0)};
	return difftime(now, lastHeartbeatTimestamp) < 60;
}
