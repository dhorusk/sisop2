#ifndef GROUP_MANAGER_HPP
#define GROUP_MANAGER_HPP

#include <string>
#include <memory>
#include <optional>
#include <unordered_map>
#include "Group.hpp"
#include "Event.hpp"
#include "LoggingHelper.hpp"

class GroupManager {
  public:
    static GroupManager& GetInstance();
    std::optional<std::shared_ptr<Group>> GetGroup(const std::string& groupName);
    bool IsGroupNameValid(const std::string& groupName);
    void AddGroup(const std::shared_ptr<Group>& group);
    bool GroupExists(const std::string& groupName);
    void BroadcastMessage(const std::string& groupName, Event* event);
    void AddUserToGroup(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor,
        const std::string& mediatorAddress,
        int mediatorPort);
    void RemoveUserFromGroup(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor,
        const std::string& mediatorAddress,
        int mediatorPort);

  private:
    GroupManager() {};
    GroupManager(GroupManager const &) = delete;
    GroupManager &operator=(GroupManager const &) = delete;

    static GroupManager instance;
    std::mutex groupsMutex;
    std::unordered_map<std::string, std::shared_ptr<Group>> groups;
    LoggingHelper logger{INFO};
    std::shared_ptr<Group> GetOrCreateGroup(const std::string& groupName);
};

#endif
