#include "Group.hpp"

using namespace std;

Group::Group(const string& groupName) : repository(groupName) {
    this->name = groupName;

    // lock both simultaneously without deadlock
    scoped_lock lock(messagesMutex, repositoryMutex);

    // load existing messages from disk, if any
    messages = repository.GetAllMessages();
}

const string& Group::GetGroupName() {
    return name;
}

const unordered_set<string>& Group::GetAllUsers() {
    return users;
}

const vector<shared_ptr<Message>>& Group::GetMessages() {
    return messages;
}

void Group::AddUser(const string& userName) {
    scoped_lock lock(usersMutex);

    logger.Info("Adding user " + userName + " to group " + name);
    users.insert(userName);
}

void Group::AddMessage(Message message, bool persistToFile) {
    logger.Info("Storing message in group " + name);

    scoped_lock lock(messagesMutex, repositoryMutex);

    auto newMessage{make_shared<Message>(message)};
    messages.push_back(newMessage);

    if (persistToFile) {
        repository.StoreMessage(newMessage.get());
    }
}

void Group::RemoveUser(const string& userName) {
    scoped_lock lock(usersMutex);

    users.erase(userName);
}

bool Group::IsUserAlreadyInTheGroup(const string& userName) {
    scoped_lock lock(usersMutex);

    auto it{users.find(userName)};

    return !(it == users.end());
}
