#include "Message.hpp"

using namespace std;

Message::Message(const string& timestamp, const string& userName, const string& messageContent) {
    this->authorUserName = userName;
    this->messageContent = messageContent;
    this->timestamp = timestamp;
}

const string& Message::GetAuthor() {
    return authorUserName;
}

const string& Message::GetContent() {
    return messageContent;
}

const string& Message::GetTimestamp() {
    return timestamp;
}
