#include <iostream>
#include "MediatorConnectionManager.hpp"
#include "LoggingHelper.hpp"

using namespace std;

int main(int argc, char* argv[]) {
	// show help if not all args were given
	if (argc < 2) {
		cout << "Usage:" << endl;
		cout << "\t" << argv[0] << " PORT" << endl;
		cout << endl;
		cout << "Where:" << endl;
		cout << "\t" << "MEDiATOR_PORT: Network port for the mediator" << endl;
		exit(EXIT_SUCCESS);
	}

	LoggingHelper logger(INFO);

	// try to parse port
	int mediatorPort{0};
	try {
		mediatorPort = stoi(argv[1]);
		logger.Info("MEDIATOR_PORT set to " + to_string(mediatorPort));
	}
	catch (...) {
		logger.Error("Invalid value for MEDiATOR_PORT");
		exit(EXIT_FAILURE);
	}

	auto& connectionManager{MediatorConnectionManager::GetInstance()};
	connectionManager.SetMediatorAddress("127.0.0.1");
	connectionManager.SetMediatorPort(mediatorPort);
	connectionManager.WaitForConnections(mediatorPort);

	return 0;
}
