#include <unistd.h>
#include <cstring>
#include <arpa/inet.h>
#include "ConnectionManager.hpp"

using namespace std;

void ConnectionManager::Send(int socketDescriptor, Event* event) {
	logger.Info("Sending event with type " + to_string(event->GetType()) + "...");

	int eventType{htonl(event->GetType())};
	logger.Debug("Going to write " + to_string(sizeof(eventType)) + " bytes to socket " + to_string(socketDescriptor));

	auto sent{write(socketDescriptor, &eventType, sizeof(eventType))};
	logger.Debug("Wrote " + to_string(sent) + " bytes to socket " + to_string(socketDescriptor));
	if (sent <= 0) {
		logger.Error("Error writing event type to socket: " + string(strerror(errno)));
		throw runtime_error("write(): " + string(strerror(errno)));
	}

	int payloadLength{htonl(event->GetMarshalledSize())};
	logger.Debug("Going to write " + to_string(sizeof(payloadLength)) + " bytes to socket " + to_string(socketDescriptor));

	sent = write(socketDescriptor, &payloadLength, sizeof(payloadLength));
	logger.Debug("Wrote " + to_string(sent) + " bytes to socket " + to_string(socketDescriptor));
	if (sent <= 0) {
		logger.Error("Error writing payload length to socket: " + string(strerror(errno)));
		throw runtime_error("write(): " + string(strerror(errno)));
	}

	logger.Info("Event payload has " + to_string(event->GetMarshalledSize()) + " bytes...");
	if (event->GetMarshalledSize() > 0) {
		auto payload{event->Marshal()};
		logger.Debug("Going to write " + to_string(event->GetMarshalledSize()) + " bytes to socket " + to_string(socketDescriptor));

		sent = write(socketDescriptor, payload.get(), event->GetMarshalledSize());
		logger.Debug("Wrote " + to_string(sent) + " bytes to socket " + to_string(socketDescriptor));
		if (sent <= 0) {
			logger.Error("Error writing payload to socket: " + string(strerror(errno)));
			throw runtime_error("write(): " + string(strerror(errno)));
		}
	}
}

shared_ptr<Event> ConnectionManager::Receive(int socketDescriptor) {
	int eventType{-1};
	logger.Debug("Going to read " + to_string(sizeof(eventType)) + " bytes from socket " + to_string(socketDescriptor));

	auto received{read(socketDescriptor, &eventType, sizeof(eventType))};
	logger.Debug("Read " + to_string(received) + " bytes from socket " + to_string(socketDescriptor));
	if (received <= 0 || eventType == -1) {
		if (errno != 0) {
			logger.Error("Error reading event type from socket: " + string(strerror(errno)));
		}
		throw runtime_error("read(): " + string(strerror(errno)));
	}

	eventType = ntohl(eventType);
	logger.Info("Receiving event with type " + to_string(eventType) + "...");

	int payloadLength{-1};
	logger.Debug("Going to read " + to_string(sizeof(payloadLength)) + " bytes from socket " + to_string(socketDescriptor));

	received = read(socketDescriptor, &payloadLength, sizeof(payloadLength));
	logger.Debug("Read " + to_string(received) + " bytes from socket " + to_string(socketDescriptor));
	if (received <= 0 || payloadLength == -1) {
		if (errno != 0) {
			logger.Error("Error reading payload length from socket: " + string(strerror(errno)));
		}
		throw runtime_error("read(): " + string(strerror(errno)));
	}

	payloadLength = ntohl(payloadLength);
	logger.Info("Event payload has " + to_string(payloadLength) + " bytes...");

	unique_ptr<char[]> payload;

	if (payloadLength > 0) {
		payload = make_unique<char[]>(payloadLength);
		logger.Debug("Going to read " + to_string(payloadLength) + " bytes from socket " + to_string(socketDescriptor));

		received = read(socketDescriptor, payload.get(), payloadLength);
		logger.Debug("Read " + to_string(received) + " bytes from socket " + to_string(socketDescriptor));
		if (received <= 0) {
			if (errno != 0) {
				logger.Error("Error reading payload from socket: " + string(strerror(errno)));
			}
			throw runtime_error("read(): " + string(strerror(errno)));
		}
	}

	auto event{Event::Unmarshal(static_cast<EventType>(eventType), payload.get())};
	if (!event) {
		logger.Error("Could not unmarshal event with type " + eventType);
		throw runtime_error("Event::Unmarshal(): failed to unmarshal eventType " + to_string(eventType));
	}

	return event.value();
}
