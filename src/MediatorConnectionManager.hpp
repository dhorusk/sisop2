#ifndef MEDIATOR_CONNECTION_MANAGER_HPP
#define MEDIATOR_CONNECTION_MANAGER_HPP

#include <memory>
#include <thread>
#include <mutex>
#include <unordered_map>
#include "ConnectionManager.hpp"
#include "LoggingHelper.hpp"

class MediatorConnectionManager : public ConnectionManager {
  public:
    static const int MAX_SIMULTANEOUS_SOCKET_CLIENTS{5};

    static MediatorConnectionManager& GetInstance();
    const std::string& GetMediatorAddress();
    void SetMediatorAddress(const std::string& mediatorAddress);
    int GetMediatorPort();
    void SetMediatorPort(int mediatorPort);
    void WaitForConnections(int mediatorPort);
    void SendToPrincipal(Event* event);
    void Send(int socketDescriptor, Event* event);
    void ListenForClientEvents(int socketDescriptor);
    void SetPrincipalSocketDescriptor(int socketDescriptor);

  private:
    MediatorConnectionManager() {};
    MediatorConnectionManager(MediatorConnectionManager const &) = delete;
    MediatorConnectionManager &operator=(MediatorConnectionManager const &) = delete;

    static MediatorConnectionManager instance;
    std::string mediatorAddress;
    int mediatorPort{-1};
    std::unordered_map<int, std::mutex> socketDescriptorMutexes;
    int principalSocketDescriptor{-1};
    std::mutex principalSocketDescriptorMutex;
    LoggingHelper logger{INFO};
};

#endif
