#ifndef UI_MANAGER_HPP
#define UI_MANAGER_HPP

#include <memory>
#include <mutex>
#include <ncurses.h>
#include "LoggingHelper.hpp"

class UIManager {
  public:
    ~UIManager();
    static UIManager& GetInstance();
    void Init(const std::string& userName, const std::string& groupName);
    void ProcessUserInput();
    void DisplayMessage(
        const std::string& author,
        const std::string& content,
        const std::string& timestamp);
    void DisplayEvent(const std::string& eventMessage, const std::string& timestamp);

  private:
    UIManager() {};
    UIManager(UIManager const &) = delete;
    UIManager &operator=(UIManager const &) = delete;

    static UIManager instance;
    static bool initialized;
    std::string userName;
    std::string groupName;
    WINDOW* chatWindowFrame{nullptr};
    WINDOW* chatWindow{nullptr};
    WINDOW* inputWindowFrame{nullptr};
    WINDOW* inputWindow{nullptr};
    std::mutex terminalMutex;
    LoggingHelper logger{ERROR};
};

#endif
