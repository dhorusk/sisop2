#ifndef CONNECTION_MANAGER_HPP
#define CONNECTION_MANAGER_HPP

#include <memory>
#include "Event.hpp"
#include "LoggingHelper.hpp"

class ConnectionManager {
  public:
    void Send(int socketDescriptor, Event* event);
    std::shared_ptr<Event> Receive(int socketDescriptor);

  private:
    LoggingHelper logger{ERROR};
};

#endif
