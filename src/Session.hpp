#ifndef SESSION_HPP
#define SESSION_HPP

#include <ctime>
#include "LoggingHelper.hpp"

class Session {
  public:
    Session(
        const std::string& userName,
        const std::string& groupName,
        int clientSocketDescriptor,
        const std::string& mediatorAddress,
        int mediatorPort);
    void UpdateHeartbeat();
    bool IsSessionActive();
    const std::string& GetUserName();
    const std::string& GetGroupName();
    int GetClientSocketDescriptor();
    const std::string& GetMediatorAddress();
    int GetMediatorPort();

  private:
    int clientSocketDescriptor{-1};
    std::string mediatorAddress;
    int mediatorPort{-1};
    std::string userName;
    std::string groupName;
    std::time_t lastHeartbeatTimestamp{0};
    LoggingHelper logger{ERROR};
};

#endif
