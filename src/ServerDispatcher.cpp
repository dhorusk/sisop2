#include <mutex>
#include <unistd.h>
#include "ServerDispatcher.hpp"
#include "SessionManager.hpp"
#include "GroupManager.hpp"
#include "ServerConnectionManager.hpp"

using namespace std;

ServerDispatcher ServerDispatcher::instance;

ServerDispatcher& ServerDispatcher::GetInstance() {
    // singleton pattern
    return instance;
}

void ServerDispatcher::Dispatch(Event* event) {
    logger.Info("Dispatching event with type " + to_string(event->GetType()));

    auto& connectionManager{ServerConnectionManager::GetInstance()};
    switch(event->GetType()) {
        case KeepAlive:
            return Dispatch(static_cast<KeepAliveEvent*>(event));
        case LoginAttempt:
            if (electedAsPrincipal) {
                connectionManager.ForwardToPeers(event);
            }
            return Dispatch(static_cast<LoginAttemptEvent*>(event));
        case LoginFailed:
            return Dispatch(static_cast<LoginFailedEvent*>(event));
        case LoginSucceeded:
            return Dispatch(static_cast<LoginSucceededEvent*>(event));
        case Logout:
            if (electedAsPrincipal) {
                connectionManager.ForwardToPeers(event);
            }
            return Dispatch(static_cast<LogoutEvent*>(event));
        case UserSentMessage:
            if (electedAsPrincipal) {
                connectionManager.ForwardToPeers(event);
            }
            return Dispatch(static_cast<UserSentMessageEvent*>(event));
        case GroupReceivedMessage:
            return Dispatch(static_cast<GroupReceivedMessageEvent*>(event));
        case UserJoinedGroup:
            return Dispatch(static_cast<UserJoinedGroupEvent*>(event));
        case UserLeftGroup:
            return Dispatch(static_cast<UserLeftGroupEvent*>(event));
        case ElectionStarted:
            return Dispatch(static_cast<ElectionStartedEvent*>(event));
        case ElectionEnded:
            return Dispatch(static_cast<ElectionEndedEvent*>(event));
    }
}

void ServerDispatcher::Dispatch(KeepAliveEvent* event) {
    logger.Info("Dispatching KeepAliveEvent");

    auto& sessionManager{SessionManager::GetInstance()};
    auto session{sessionManager.GetSessionByIdentifiers(
        event->GetClientSocketDescriptor(),
        event->GetMediatorAddress(),
        event->GetMediatorPort())};
    if (!session) {
        logger.Error("Failed to locate session related to clientSocketDescriptor " +
            to_string(event->GetClientSocketDescriptor()) + " and mediatorAddress " +
            event->GetMediatorAddress() + ":" + to_string(event->GetMediatorPort()));
        return;
    }

    session.value()->UpdateHeartbeat();
}

void ServerDispatcher::Dispatch(LoginAttemptEvent* event) {
    logger.Info("Dispatching login attempt event");
    logger.Info("User: " + event->GetUserName());
    logger.Info("Group: " + event->GetGroupName());
    logger.Info("Client socket descriptor: " + to_string(event->GetClientSocketDescriptor()));
    logger.Info("Mediator address: " + event->GetMediatorAddress() + ":" + to_string(event->GetMediatorPort()));

    auto& sessionManager{SessionManager::GetInstance()};
    sessionManager.HandleLoginAttempt(event);
}

void ServerDispatcher::Dispatch(LoginFailedEvent* event) {
    if (!electedAsPrincipal) {
        logger.Info("Would have dispatched login failed event");
        return;
    }

    logger.Info("Dispatching login failed event");

    auto& connectionManager{ServerConnectionManager::GetInstance()};
    int mediatorSocketDescriptor = connectionManager.GetMediatorSocketDescriptorByAddressAndPort(
        event->GetMediatorAddress(),
        event->GetMediatorPort());
    connectionManager.Send(mediatorSocketDescriptor, event);
}

void ServerDispatcher::Dispatch(LoginSucceededEvent* event) {
    if (!electedAsPrincipal) {
        logger.Info("Would have dispatched login success event");
        return;
    }

    logger.Info("Dispatching login success event");

    auto& connectionManager{ServerConnectionManager::GetInstance()};
    int mediatorSocketDescriptor = connectionManager.GetMediatorSocketDescriptorByAddressAndPort(
        event->GetMediatorAddress(),
        event->GetMediatorPort());
    connectionManager.Send(mediatorSocketDescriptor, event);
}

void ServerDispatcher::Dispatch(LogoutEvent* event) {
    logger.Info("Dispatching logout event");
    auto& sessionManager{SessionManager::GetInstance()};
    sessionManager.HandleLogout(event);
}

void ServerDispatcher::Dispatch(UserSentMessageEvent* event) {
    logger.Info("Dispatching UserSentMessage event");

    auto groupReceivedMessageEvent{make_shared<GroupReceivedMessageEvent>(
        event->GetUserName(),
        event->GetGroupName(),
        event->GetMessageContent())};
    Dispatch(groupReceivedMessageEvent.get());
}

void ServerDispatcher::Dispatch(GroupReceivedMessageEvent* event) {
    logger.Info("Dispatching GroupReceivedMessage event");

    auto& groupManager{GroupManager::GetInstance()};
    auto group{groupManager.GetGroup(event->GetGroupName())};
    if (!group) {
        logger.Error("Attempting to broadcast message to inexistent group " + event->GetGroupName());
        return;
    }

    // store message in group
    Message message(event->GetTimestamp(), event->GetUserName(), event->GetMessageContent());
    group.value()->AddMessage(message, electedAsPrincipal);

    event->SetCurrentTimestamp();
    groupManager.BroadcastMessage(event->GetGroupName(), event);
}

void ServerDispatcher::Dispatch(UserJoinedGroupEvent* event) {
    if (!electedAsPrincipal) {
        logger.Info("Would have dispatched user joined group event");
        return;
    }

    logger.Info("Dispatching user joined group event to group " + event->GetGroupName());

    auto& connectionManager{ServerConnectionManager::GetInstance()};
    auto& groupManager{GroupManager::GetInstance()};

    auto group{groupManager.GetGroup(event->GetGroupName())};
    if (group) {
        auto messages{group.value()->GetMessages()};

        // Gets the last n messages to send based on the parameter passed when initializing the server
        vector<shared_ptr<Message>> lastMessages;
        if (messages.size() <= connectionManager.GetMaxMessagesToSend()) {
            lastMessages = messages;
        } else {
            copy(messages.end() - connectionManager.GetMaxMessagesToSend(), messages.end(), back_inserter(lastMessages));
        }

        for (const auto& message : lastMessages)  {
            auto oldMessageEvent{make_shared<GroupReceivedMessageEvent>(
                message->GetAuthor(),
                event->GetGroupName(),
                message->GetContent(),
                event->GetClientSocketDescriptor())};
            oldMessageEvent->SetTimestamp(message->GetTimestamp());
            int mediatorSocketDescriptor = connectionManager.GetMediatorSocketDescriptorByAddressAndPort(
                event->GetMediatorAddress(),
                event->GetMediatorPort());
            connectionManager.Send(mediatorSocketDescriptor, oldMessageEvent.get());
        }
    }

    groupManager.BroadcastMessage(event->GetGroupName(), event);
}

void ServerDispatcher::Dispatch(UserLeftGroupEvent* event) {
    if (!electedAsPrincipal) {
        logger.Info("Would have dispatched user left group event");
        return;
    }

    logger.Info("Dispatching user left group event to group " + event->GetGroupName());

    auto& groupManager{GroupManager::GetInstance()};
    groupManager.BroadcastMessage(event->GetGroupName(), event);
}

void ServerDispatcher::Dispatch(ElectionStartedEvent* event) {
    logger.Info("Dispatching election started event");
    logger.Info("Candidate is " + event->GetCandidateAddress() + ":" + to_string(event->GetCandidatePort()));

    scoped_lock lock(electionStateMutex);

    if (electedAsPrincipal) {
        electedAsPrincipal = false;
        logger.Info("No longer marked as principal due to the new election");
    }

	auto& connectionManager{ServerConnectionManager::GetInstance()};
    auto serverAddress = connectionManager.GetServerAddress();
    auto serverPort = connectionManager.GetServerPort();
    int peerSocketDescriptor = connectionManager.ConnectToRingPeer();

    if (event->GetCandidateAddress().compare(serverAddress) > 0
            || event->GetCandidatePort() > serverPort) {
        // other process has priority, so just forward its message to the next peer
        logger.Info("Lost the election");
        electionOngoing = true;
        connectionManager.Send(peerSocketDescriptor, event);
    }
    else if (event->GetCandidateAddress().compare(serverAddress) != 0
            || event->GetCandidatePort() != serverPort) {
        // this process has priority
        if (!electionOngoing) {
            logger.Info("Entering an election");
            electionOngoing = true;

	        auto electionEndedEvent{make_unique<ElectionStartedEvent>(serverAddress, serverPort)};
            connectionManager.Send(peerSocketDescriptor, electionEndedEvent.get());
        }
        else {
            logger.Info("Still in the election");
        }
    }
    else {
        // Since the current process received its own pid in an election message,
        // it knows that it is the principal, so it sends an "elected" event around the ring
        logger.Info("Won the election, telling others");
	    auto electionEndedEvent{make_unique<ElectionEndedEvent>(serverAddress, serverPort)};
        connectionManager.Send(peerSocketDescriptor, electionEndedEvent.get());
    }

    close(peerSocketDescriptor);
}

void ServerDispatcher::Dispatch(ElectionEndedEvent* event) {
    logger.Info("Dispatching election ended event");
    logger.Info("New principal is " + event->GetPrincipalAddress() + ":" + to_string(event->GetPrincipalPort()));

    scoped_lock lock(electionStateMutex);

    logger.Info("Election is over");
    electionOngoing = false;

    auto& connectionManager{ServerConnectionManager::GetInstance()};
    if (event->GetPrincipalAddress() == connectionManager.GetServerAddress()
            && event->GetPrincipalPort() == connectionManager.GetServerPort()) {
        // if it's our own elected event, everyone in the ring was notified
        // so set ourselves as principal and notify all the front-end servers
        logger.Info("Setting itself as principal");
        electedAsPrincipal = true;
        connectionManager.ConnectToMediatorsAsPrincipal();
    }
    else {
        // if not, write it down and pass it around the ring
        logger.Info("Setting " + event->GetPrincipalAddress() + ":"
            + to_string(event->GetPrincipalPort()) + " as principal");
        connectionManager.StartMonitoringPrincipal(event->GetPrincipalAddress(), event->GetPrincipalPort());
        int peerSocketDescriptor = connectionManager.ConnectToRingPeer();
        connectionManager.Send(peerSocketDescriptor, event);
    }
}

unique_lock<mutex> ServerDispatcher::LockElectionState() {
    unique_lock lock(electionStateMutex);
    return lock;
}

bool ServerDispatcher::GetElectionOngoing() {
    return electionOngoing;
}

void ServerDispatcher::SetElectionOngoing(bool electionOngoing) {
    this->electionOngoing = electionOngoing;
}

bool ServerDispatcher::GetElectedAsPrincipal() {
    return electedAsPrincipal;
}

void ServerDispatcher::SetElectedAsPrincipal(bool electedAsPrincipal) {
    this->electedAsPrincipal = electedAsPrincipal;
}
