#include <regex>
#include <algorithm>
#include "UserManager.hpp"
#include "Event.hpp"

using namespace std;

UserManager UserManager::instance;

UserManager& UserManager::GetInstance() {
    // singleton pattern
    return instance;
}

const unordered_set<string>& UserManager::GetAllUsers() {
    return users;
}

void UserManager::AddUser(const string& userName) {
    logger.Info("Creating user " + userName);

    if (find(users.begin(), users.end(), userName) != users.end() ) {
        users.insert(userName);
    }
}

void UserManager::RemoveUser(const string& userName) {
    auto it{find(users.begin(), users.end(), userName)};
    if (it != users.end()) {
        users.erase(it);
    }
}

bool UserManager::IsUserNameValid(const string& userName) {
    // username must have between 4 and 20 characters
    // first character must be a letter
    // the rest can be either a letter, a number or a dot (.)
    regex validUserNameRegex("^[a-zA-Z][a-zA-Z0-9.]{3," + to_string(Event::USER_NAME_LENGTH - 1) + "}$");
    return regex_match(userName, validUserNameRegex);
}
