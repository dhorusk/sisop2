#ifndef MEDIATOR_DISPATCHER_HPP
#define MEDIATOR_DISPATCHER_HPP

#include "Event.hpp"
#include "LoggingHelper.hpp"

class MediatorDispatcher {
  public:
    static MediatorDispatcher& GetInstance();
    void Dispatch(Event* event);
    void Dispatch(KeepAliveEvent* event);
    void Dispatch(LoginAttemptEvent* event);
    void Dispatch(LoginFailedEvent* event);
    void Dispatch(LoginSucceededEvent* event);
    void Dispatch(LogoutEvent* event);
    void Dispatch(UserSentMessageEvent* event);
    void Dispatch(GroupReceivedMessageEvent* event);
    void Dispatch(UserJoinedGroupEvent* event);
    void Dispatch(UserLeftGroupEvent* event);
    void Dispatch(ElectionEndedEvent* event);

  private:
    MediatorDispatcher() {};
    MediatorDispatcher(MediatorDispatcher const &) = delete;
    MediatorDispatcher &operator=(MediatorDispatcher const &) = delete;

    static MediatorDispatcher instance;
    LoggingHelper logger{INFO};
};

#endif
