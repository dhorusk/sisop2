#include "Event.hpp"

using namespace std;

#pragma region Event

optional<shared_ptr<Event>> Event::Unmarshal(EventType eventType, void* payload) {
    switch(eventType) {
        case KeepAlive:
            return KeepAliveEvent::Unmarshal(reinterpret_cast<KeepAliveStruct*>(payload));
        case LoginAttempt:
            return LoginAttemptEvent::Unmarshal(reinterpret_cast<LoginAttemptStruct*>(payload));
        case LoginFailed:
            return LoginFailedEvent::Unmarshal(reinterpret_cast<LoginFailedStruct*>(payload));
        case LoginSucceeded:
            return LoginSucceededEvent::Unmarshal(reinterpret_cast<LoginSucceededStruct*>(payload));
        case Logout:
            return LogoutEvent::Unmarshal(reinterpret_cast<LogoutStruct*>(payload));
        case UserSentMessage:
            return UserSentMessageEvent::Unmarshal(reinterpret_cast<UserSentMessageStruct*>(payload));
        case GroupReceivedMessage:
            return GroupReceivedMessageEvent::Unmarshal(reinterpret_cast<GroupReceivedMessageStruct*>(payload));
        case UserJoinedGroup:
            return UserJoinedGroupEvent::Unmarshal(reinterpret_cast<UserJoinedGroupStruct*>(payload));
        case UserLeftGroup:
            return UserLeftGroupEvent::Unmarshal(reinterpret_cast<UserLeftGroupStruct*>(payload));
        case ElectionStarted:
            return ElectionStartedEvent::Unmarshal(reinterpret_cast<ElectionStartedStruct*>(payload));
        case ElectionEnded:
            return ElectionEndedEvent::Unmarshal(reinterpret_cast<ElectionEndedStruct*>(payload));
    }

    return {};
}

int Event::GetClientSocketDescriptor() {
    return clientSocketDescriptor;
}

void Event::SetClientSocketDescriptor(int clientSocketDescriptor) {
    this->clientSocketDescriptor = clientSocketDescriptor;
}

const string& Event::GetMediatorAddress() {
    return mediatorAddress;
}

void Event::SetMediatorAddress(const string& mediatorAddress) {
    this->mediatorAddress = mediatorAddress;
}

int Event::GetMediatorPort() {
    return mediatorPort;
}

void Event::SetMediatorPort(int mediatorPort) {
    this->mediatorPort = mediatorPort;
}

const string& Event::GetTimestamp() {
    return timestamp;
}

void Event::SetCurrentTimestamp() {
    char formatted[24];

    auto now{time(0)};
	strftime(formatted, sizeof(formatted), "%Y-%m-%d %H:%M:%S", localtime(&now));

    this->timestamp = formatted;
}

void Event::SetTimestamp(const string& timestamp) {
    this->timestamp = timestamp;
}

#pragma endregion


#pragma region KeepAlive

KeepAliveEvent::KeepAliveEvent(
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    this->clientSocketDescriptor = clientSocketDescriptor;
    this->mediatorAddress = mediatorAddress;
    this->mediatorPort = mediatorPort;
}

EventType KeepAliveEvent::GetType() {
    return KeepAlive;
}

int KeepAliveEvent::GetMarshalledSize() {
    return sizeof(KeepAliveStruct);
}

shared_ptr<void> KeepAliveEvent::Marshal() {
    auto st{make_shared<KeepAliveStruct>()};

    st->clientSocketDescriptor = clientSocketDescriptor;
    mediatorAddress.copy(st->mediatorAddress, SERVER_ADDRESS_LENGTH);
    st->mediatorPort = mediatorPort;

    return static_pointer_cast<void>(st);
}

shared_ptr<KeepAliveEvent> KeepAliveEvent::Unmarshal(KeepAliveStruct* payload) {
    return make_shared<KeepAliveEvent>(
        payload->clientSocketDescriptor,
        payload->mediatorAddress,
        payload->mediatorPort);
}

#pragma endregion


#pragma region LoginAttempt

LoginAttemptEvent::LoginAttemptEvent(
        const string& userName,
        const string& groupName,
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    this->userName = userName;
    this->groupName = groupName;
    this->clientSocketDescriptor = clientSocketDescriptor;
    this->mediatorAddress = mediatorAddress;
    this->mediatorPort = mediatorPort;
}

const string& LoginAttemptEvent::GetUserName() {
    return userName;
}

const string& LoginAttemptEvent::GetGroupName() {
    return groupName;
}

EventType LoginAttemptEvent::GetType() {
    return LoginAttempt;
}

int LoginAttemptEvent::GetMarshalledSize() {
    return sizeof(LoginAttemptStruct);
}

shared_ptr<void> LoginAttemptEvent::Marshal() {
    auto st{make_shared<LoginAttemptStruct>()};

    userName.copy(st->userName, USER_NAME_LENGTH);
    groupName.copy(st->groupName, GROUP_NAME_LENGTH);
    st->clientSocketDescriptor = clientSocketDescriptor;
    mediatorAddress.copy(st->mediatorAddress, SERVER_ADDRESS_LENGTH);
    st->mediatorPort = mediatorPort;

    return static_pointer_cast<void>(st);
}

shared_ptr<LoginAttemptEvent> LoginAttemptEvent::Unmarshal(LoginAttemptStruct* payload) {
    return make_shared<LoginAttemptEvent>(
        payload->userName,
        payload->groupName,
        payload->clientSocketDescriptor,
        payload->mediatorAddress,
        payload->mediatorPort);
}

#pragma endregion


#pragma region LoginFailed

LoginFailedEvent::LoginFailedEvent(const string& reason, int clientSocketDescriptor) {
    this->reason = reason;
    this->clientSocketDescriptor = clientSocketDescriptor;
}

const string& LoginFailedEvent::GetReason() {
    return reason;
}

EventType LoginFailedEvent::GetType() {
    return LoginFailed;
}

int LoginFailedEvent::GetMarshalledSize() {
    return sizeof(LoginFailedStruct);
}

shared_ptr<void> LoginFailedEvent::Marshal() {
    auto st{make_shared<LoginFailedStruct>()};

    reason.copy(st->reason, FAILURE_REASON_LENGTH);
    st->clientSocketDescriptor = clientSocketDescriptor;

    return static_pointer_cast<void>(st);
}

shared_ptr<LoginFailedEvent> LoginFailedEvent::Unmarshal(LoginFailedStruct* payload) {
    return make_shared<LoginFailedEvent>(payload->reason, payload->clientSocketDescriptor);
}

#pragma endregion


#pragma region LoginSucceeded

LoginSucceededEvent::LoginSucceededEvent(int clientSocketDescriptor) {
    this->clientSocketDescriptor = clientSocketDescriptor;
}

EventType LoginSucceededEvent::GetType() {
    return LoginSucceeded;
}

int LoginSucceededEvent::GetMarshalledSize() {
    return sizeof(LoginSucceededStruct);
}

shared_ptr<void> LoginSucceededEvent::Marshal() {
    auto st{make_shared<LoginSucceededStruct>()};

    st->clientSocketDescriptor = clientSocketDescriptor;

    return static_pointer_cast<void>(st);
}

shared_ptr<LoginSucceededEvent> LoginSucceededEvent::Unmarshal(LoginSucceededStruct* payload) {
    return make_shared<LoginSucceededEvent>(
        payload->clientSocketDescriptor);
}

#pragma endregion


#pragma region Logout

LogoutEvent::LogoutEvent(
        const string& userName,
        const string& groupName,
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    this->userName = userName;
    this->groupName = groupName;
    this->clientSocketDescriptor = clientSocketDescriptor;
    this->mediatorAddress = mediatorAddress;
    this->mediatorPort = mediatorPort;
}

const string& LogoutEvent::GetGroupName() {
    return groupName;    
}

const string& LogoutEvent::GetUserName() {
    return userName;
}

EventType LogoutEvent::GetType() {
    return Logout;
}

int LogoutEvent::GetMarshalledSize() {
    return sizeof(LogoutStruct);
}

shared_ptr<void> LogoutEvent::Marshal() {
    auto st{make_shared<LogoutStruct>()};

    userName.copy(st->userName, USER_NAME_LENGTH);
    groupName.copy(st->groupName, GROUP_NAME_LENGTH);
    st->clientSocketDescriptor = clientSocketDescriptor;
    mediatorAddress.copy(st->mediatorAddress, SERVER_ADDRESS_LENGTH);
    st->mediatorPort = mediatorPort;

    return static_pointer_cast<void>(st);
}

shared_ptr<LogoutEvent> LogoutEvent::Unmarshal(LogoutStruct* payload) {
    return make_shared<LogoutEvent>(
        payload->userName,
        payload->groupName,
        payload->clientSocketDescriptor,
        payload->mediatorAddress,
        payload->mediatorPort);
}

#pragma endregion


#pragma region UserSentMessage

UserSentMessageEvent::UserSentMessageEvent(
        const string& userName,
        const string& groupName,
        const string& messageContent,
        int clientSocketDescriptor,
        const string& mediatorAddress,
        int mediatorPort) {
    this->userName = userName;
    this->groupName = groupName;
    this->messageContent = messageContent;
    this->clientSocketDescriptor = clientSocketDescriptor;
    this->mediatorAddress = mediatorAddress;
    this->mediatorPort = mediatorPort;
}

const string& UserSentMessageEvent::GetMessageContent() {
    return messageContent;
}

const string& UserSentMessageEvent::GetUserName() {
    return userName;
}

const string& UserSentMessageEvent::GetGroupName() {
    return groupName;
}

EventType UserSentMessageEvent::GetType() {
    return UserSentMessage;
}

int UserSentMessageEvent::GetMarshalledSize() {
    return sizeof(UserSentMessageStruct);
}

shared_ptr<void> UserSentMessageEvent::Marshal() {
    auto st{make_shared<UserSentMessageStruct>()};

    messageContent.copy(st->messageContent, MESSAGE_CONTENT_LENGTH);
    userName.copy(st->userName, USER_NAME_LENGTH);
    groupName.copy(st->groupName, GROUP_NAME_LENGTH);
    st->clientSocketDescriptor = clientSocketDescriptor;
    mediatorAddress.copy(st->mediatorAddress, SERVER_ADDRESS_LENGTH);
    st->mediatorPort = mediatorPort;

    return static_pointer_cast<void>(st);
}

shared_ptr<UserSentMessageEvent> UserSentMessageEvent::Unmarshal(UserSentMessageStruct* payload) {
    return make_shared<UserSentMessageEvent>(
        payload->userName,
        payload->groupName,
        payload->messageContent,
        payload->clientSocketDescriptor,
        payload->mediatorAddress,
        payload->mediatorPort);
}

#pragma endregion


#pragma region GroupReceivedMessage

GroupReceivedMessageEvent::GroupReceivedMessageEvent(
        const string& userName,
        const string& groupName,
        const string& messageContent,
        int clientSocketDescriptor) {
    this->userName = userName;
    this->groupName = groupName;
    this->messageContent = messageContent;
    this->clientSocketDescriptor = clientSocketDescriptor;
    SetCurrentTimestamp();
}

GroupReceivedMessageEvent::GroupReceivedMessageEvent(
        const string& userName,
        const string& groupName,
        const string& messageContent,
        const string& timestamp,
        int clientSocketDescriptor) {
    this->userName = userName;
    this->groupName = groupName;
    this->messageContent = messageContent;
    this->timestamp = timestamp;
    this->clientSocketDescriptor = clientSocketDescriptor;
}

const string& GroupReceivedMessageEvent::GetMessageContent() {
    return messageContent;
}

const string& GroupReceivedMessageEvent::GetUserName() {
    return userName;
}

const string& GroupReceivedMessageEvent::GetGroupName() {
    return groupName;
}

const string& GroupReceivedMessageEvent::GetTimestamp() {
    return timestamp;
}

EventType GroupReceivedMessageEvent::GetType() {
    return GroupReceivedMessage;
}

int GroupReceivedMessageEvent::GetMarshalledSize() {
    return sizeof(GroupReceivedMessageStruct);
}

shared_ptr<void> GroupReceivedMessageEvent::Marshal() {
    auto st{make_shared<GroupReceivedMessageStruct>()};

    userName.copy(st->userName, USER_NAME_LENGTH);
    groupName.copy(st->groupName, GROUP_NAME_LENGTH);
    messageContent.copy(st->messageContent, MESSAGE_CONTENT_LENGTH);
    st->clientSocketDescriptor = clientSocketDescriptor;
    timestamp.copy(st->timestamp, TIMESTAMP_LENGTH);

    return static_pointer_cast<void>(st);
}

shared_ptr<GroupReceivedMessageEvent> GroupReceivedMessageEvent::Unmarshal(GroupReceivedMessageStruct* payload) {
    return make_shared<GroupReceivedMessageEvent>(
        payload->userName,
        payload->groupName,
        payload->messageContent,
        payload->timestamp,
        payload->clientSocketDescriptor);
}

#pragma endregion


#pragma region UserJoinedGroup

UserJoinedGroupEvent::UserJoinedGroupEvent(
        const string& userName,
        const string& groupName,
        int clientSocketDescriptor) {
    this->userName = userName;
    this->groupName = groupName;
    this->clientSocketDescriptor = clientSocketDescriptor;
    SetCurrentTimestamp();
}

UserJoinedGroupEvent::UserJoinedGroupEvent(
        const string& userName,
        const string& groupName,
        const string& timestamp,
        int clientSocketDescriptor) {
    this->userName = userName;
    this->groupName = groupName;
    this->timestamp = timestamp;
    this->clientSocketDescriptor = clientSocketDescriptor;
}

const string& UserJoinedGroupEvent::GetUserName() {
    return userName;
}

const string& UserJoinedGroupEvent::GetGroupName() {
    return groupName;
}

const string& UserJoinedGroupEvent::GetTimestamp() {
    return timestamp;
}

EventType UserJoinedGroupEvent::GetType() {
    return UserJoinedGroup;
}

int UserJoinedGroupEvent::GetMarshalledSize() {
    return sizeof(UserJoinedGroupStruct);
}

shared_ptr<void> UserJoinedGroupEvent::Marshal() {
    auto st{make_shared<UserJoinedGroupStruct>()};

    userName.copy(st->userName, USER_NAME_LENGTH);
    groupName.copy(st->groupName, GROUP_NAME_LENGTH);
    st->clientSocketDescriptor = clientSocketDescriptor;
    timestamp.copy(st->timestamp, TIMESTAMP_LENGTH);

    return static_pointer_cast<void>(st);
}

shared_ptr<UserJoinedGroupEvent> UserJoinedGroupEvent::Unmarshal(UserJoinedGroupStruct* payload) {
    return make_shared<UserJoinedGroupEvent>(
        payload->userName,
        payload->groupName,
        payload->timestamp,
        payload->clientSocketDescriptor);
}

#pragma endregion


#pragma region UserLeftGroup

UserLeftGroupEvent::UserLeftGroupEvent(
        const string& userName,
        const string& groupName,
        int clientSocketDescriptor) {
    this->userName = userName;
    this->groupName = groupName;
    this->clientSocketDescriptor = clientSocketDescriptor;
    SetCurrentTimestamp();
}

UserLeftGroupEvent::UserLeftGroupEvent(
        const string& userName,
        const string& groupName,
        const string& timestamp,
        int clientSocketDescriptor) {
    this->userName = userName;
    this->groupName = groupName;
    this->timestamp = timestamp;
    this->clientSocketDescriptor = clientSocketDescriptor;
}

const string& UserLeftGroupEvent::GetUserName() {
    return userName;
}

const string& UserLeftGroupEvent::GetGroupName() {
    return groupName;
}

const string& UserLeftGroupEvent::GetTimestamp() {
    return timestamp;
}

EventType UserLeftGroupEvent::GetType() {
    return UserLeftGroup;
}

int UserLeftGroupEvent::GetMarshalledSize() {
    return sizeof(UserLeftGroupStruct);
}

shared_ptr<void> UserLeftGroupEvent::Marshal() {
    auto st{make_shared<UserLeftGroupStruct>()};

    userName.copy(st->userName, USER_NAME_LENGTH);
    groupName.copy(st->groupName, GROUP_NAME_LENGTH);
    st->clientSocketDescriptor = clientSocketDescriptor;
    timestamp.copy(st->timestamp, TIMESTAMP_LENGTH);

    return static_pointer_cast<void>(st);
}

shared_ptr<UserLeftGroupEvent> UserLeftGroupEvent::Unmarshal(UserLeftGroupStruct* payload) {
    return make_shared<UserLeftGroupEvent>(
        payload->userName,
        payload->groupName,
        payload->timestamp,
        payload->clientSocketDescriptor);
}

#pragma endregion


#pragma region ElectionStarted

ElectionStartedEvent::ElectionStartedEvent(const std::string& candidateAddress, int candidatePort) {
    this->candidateAddress = candidateAddress;
    this->candidatePort = candidatePort;
}

EventType ElectionStartedEvent::GetType() {
    return ElectionStarted;
}

int ElectionStartedEvent::GetMarshalledSize() {
    return sizeof(ElectionStartedStruct);
}

shared_ptr<void> ElectionStartedEvent::Marshal() {
    auto st{make_shared<ElectionStartedStruct>()};

    candidateAddress.copy(st->candidateAddress, SERVER_ADDRESS_LENGTH);
    st->candidatePort = candidatePort;

    return static_pointer_cast<void>(st);
}

shared_ptr<ElectionStartedEvent> ElectionStartedEvent::Unmarshal(ElectionStartedStruct* payload) {
    return make_shared<ElectionStartedEvent>(payload->candidateAddress, payload->candidatePort);
}

const string& ElectionStartedEvent::GetCandidateAddress() {
    return candidateAddress;
}

void ElectionStartedEvent::SetCandidateAddress(const string& candidateAddress) {
    this->candidateAddress = candidateAddress;
}

int ElectionStartedEvent::GetCandidatePort() {
    return candidatePort;
}

void ElectionStartedEvent::SetCandidatePort(int candidatePort) {
    this->candidatePort = candidatePort;
}

#pragma endregion


#pragma region ElectionEnded

ElectionEndedEvent::ElectionEndedEvent(const std::string& principalAddress, int principalPort) {
    this->principalAddress = principalAddress;
    this->principalPort = principalPort;
}

EventType ElectionEndedEvent::GetType() {
    return ElectionEnded;
}

int ElectionEndedEvent::GetMarshalledSize() {
    return sizeof(ElectionEndedStruct);
}

shared_ptr<void> ElectionEndedEvent::Marshal() {
    auto st{make_shared<ElectionEndedStruct>()};

    principalAddress.copy(st->principalAddress, SERVER_ADDRESS_LENGTH);
    st->principalPort = principalPort;

    return static_pointer_cast<void>(st);
}

shared_ptr<ElectionEndedEvent> ElectionEndedEvent::Unmarshal(ElectionEndedStruct* payload) {
    return make_shared<ElectionEndedEvent>(payload->principalAddress, payload->principalPort);
}

const string& ElectionEndedEvent::GetPrincipalAddress() {
    return principalAddress;
}

void ElectionEndedEvent::SetPrincipalAddress(const string& principalAddress) {
    this->principalAddress = principalAddress;
}

int ElectionEndedEvent::GetPrincipalPort() {
    return principalPort;
}

void ElectionEndedEvent::SetPrincipalPort(int principalPort) {
    this->principalPort = principalPort;
}

#pragma endregion
