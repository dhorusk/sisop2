#ifndef LOGGING_HELPER_HPP
#define LOGGING_HELPER_HPP

#include <iostream>

enum LoggingLevel {
  // don't reorder
  NONE,
  ERROR,
  WARN,
  INFO,
  DEBUG,
};

class LoggingHelper {
  public:
    LoggingHelper(LoggingLevel level);
    LoggingHelper(LoggingLevel level, const std::string& fileName);

    void Debug(const std::string& message);
    void Info(const std::string& message);
    void Warning(const std::string& message);
    void Error(const std::string& message);

  private:
    LoggingLevel level{ERROR};
    bool logToFile{false};
    std::string logFileName;

    void Log(std::ostream& stream, const std::string& message);
    void LogToFile(const std::string& message);
};

#endif
