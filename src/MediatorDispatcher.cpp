#include "MediatorDispatcher.hpp"
#include "MediatorConnectionManager.hpp"

using namespace std;

MediatorDispatcher MediatorDispatcher::instance;

MediatorDispatcher& MediatorDispatcher::GetInstance() {
    // singleton pattern
    return instance;
}

void MediatorDispatcher::Dispatch(Event* event) {
    logger.Info("Dispatching event with type " + to_string(event->GetType()));

    switch(event->GetType()) {
        case KeepAlive:
            return Dispatch(static_cast<KeepAliveEvent*>(event));
        case LoginAttempt:
            return Dispatch(static_cast<LoginAttemptEvent*>(event));
        case LoginFailed:
            return Dispatch(static_cast<LoginFailedEvent*>(event));
        case LoginSucceeded:
            return Dispatch(static_cast<LoginSucceededEvent*>(event));
        case Logout:
            return Dispatch(static_cast<LogoutEvent*>(event));
        case UserSentMessage:
            return Dispatch(static_cast<UserSentMessageEvent*>(event));
        case GroupReceivedMessage:
            return Dispatch(static_cast<GroupReceivedMessageEvent*>(event));
        case UserJoinedGroup:
            return Dispatch(static_cast<UserJoinedGroupEvent*>(event));
        case UserLeftGroup:
            return Dispatch(static_cast<UserLeftGroupEvent*>(event));
        case ElectionStarted:
            return; // mediator should never receive this event
        case ElectionEnded:
            return Dispatch(static_cast<ElectionEndedEvent*>(event));
    }
}

void MediatorDispatcher::Dispatch(KeepAliveEvent* event) {
    logger.Info("Dispatching KeepAliveEvent");

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.SendToPrincipal(event);
}

void MediatorDispatcher::Dispatch(LoginAttemptEvent* event) {
    logger.Info("Dispatching login attempt event");
    logger.Info("User: " + event->GetUserName());
    logger.Info("Group: " + event->GetGroupName());
    logger.Info("Client socket descriptor: " + to_string(event->GetClientSocketDescriptor()));

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.SendToPrincipal(event);
}

void MediatorDispatcher::Dispatch(LoginFailedEvent* event) {
    logger.Info("Dispatching login failed event");

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.Send(event->GetClientSocketDescriptor(), event);
}

void MediatorDispatcher::Dispatch(LoginSucceededEvent* event) {
    logger.Info("Dispatching login success event");
    logger.Info("Client socket descriptor: " + to_string(event->GetClientSocketDescriptor()));

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.Send(event->GetClientSocketDescriptor(), event);
}

void MediatorDispatcher::Dispatch(LogoutEvent* event) {
    logger.Info("Dispatching logout event");

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.SendToPrincipal(event);
}

void MediatorDispatcher::Dispatch(UserSentMessageEvent* event) {
    logger.Info("Dispatching UserSentMessage event");

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.SendToPrincipal(event);
}

void MediatorDispatcher::Dispatch(GroupReceivedMessageEvent* event) {
    logger.Info("Dispatching GroupReceivedMessage event");

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.Send(event->GetClientSocketDescriptor(), event);
}

void MediatorDispatcher::Dispatch(UserJoinedGroupEvent* event) {
    logger.Info("Dispatching user joined group event to group " + event->GetGroupName());

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.Send(event->GetClientSocketDescriptor(), event);
}

void MediatorDispatcher::Dispatch(UserLeftGroupEvent* event) {
    logger.Info("Dispatching user left group event to group " + event->GetGroupName());

    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.Send(event->GetClientSocketDescriptor(), event);
}

void MediatorDispatcher::Dispatch(ElectionEndedEvent* event) {
    logger.Info("Dispatching election ended event");
    logger.Info("New principal is " + event->GetPrincipalAddress() + ":" + to_string(event->GetPrincipalPort()));

    // get new principal socket descriptor from the event
    // note it's the client socket descriptor, because we thought this was a client
    auto& connectionManager{MediatorConnectionManager::GetInstance()};
    connectionManager.SetPrincipalSocketDescriptor(event->GetClientSocketDescriptor());
}
