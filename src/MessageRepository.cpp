#include <filesystem>
#include "../lib/csv.hpp"
#include "MessageRepository.hpp"

using namespace std;
using namespace csv;

MessageRepository::MessageRepository(const string& groupName) {
    this->groupName = groupName;
    this->fileName = "./data/" + groupName + ".csv";
}

vector<shared_ptr<Message>> MessageRepository::GetAllMessages() {
    logger.Info("Loading messages for group " + groupName);

    vector<shared_ptr<Message>> messages;

    // create file if it doesn't exist
    if (!filesystem::exists(fileName)) {
        ofstream file(fileName, ios::app);
        auto writer{make_csv_writer(file)};
        writer << vector<string>({
            "timestamp",
            "author",
            "messageContent"});
    }
    else { // load file if it does exist
        CSVReader reader(fileName);
        for (const auto& row : reader) {
            logger.Debug("Loading a message!");

            string timestamp(row["timestamp"].get<>());
            string author(row["author"].get<>());
            string messageContent(row["messageContent"].get<>());

            auto message{make_shared<Message>(timestamp, author, messageContent)};
            messages.push_back(message);
        }
    }

    return messages;
}

void MessageRepository::StoreMessage(Message* message) {
    logger.Info("Storing message for group " + groupName);

	ofstream file(fileName, ios::app);
    auto writer{make_csv_writer(file)};
    writer << vector<string>({
        message->GetTimestamp(),
        message->GetAuthor(),
        message->GetContent()});
}
