#ifndef SERVER_DISPATCHER_HPP
#define SERVER_DISPATCHER_HPP

#include "Event.hpp"
#include "LoggingHelper.hpp"

class ServerDispatcher {
  public:
    static ServerDispatcher& GetInstance();
    void Dispatch(Event* event);
    void Dispatch(KeepAliveEvent* event);
    void Dispatch(LoginAttemptEvent* event);
    void Dispatch(LoginFailedEvent* event);
    void Dispatch(LoginSucceededEvent* event);
    void Dispatch(LogoutEvent* event);
    void Dispatch(UserSentMessageEvent* event);
    void Dispatch(GroupReceivedMessageEvent* event);
    void Dispatch(UserJoinedGroupEvent* event);
    void Dispatch(UserLeftGroupEvent* event);
    void Dispatch(ElectionStartedEvent* event);
    void Dispatch(ElectionEndedEvent* event);
    std::unique_lock<std::mutex> LockElectionState();
    bool GetElectionOngoing();
    void SetElectionOngoing(bool electionOngoing);
    bool GetElectedAsPrincipal();
    void SetElectedAsPrincipal(bool electedAsPrincipal);

  private:
    ServerDispatcher() {};
    ServerDispatcher(ServerDispatcher const &) = delete;
    ServerDispatcher &operator=(ServerDispatcher const &) = delete;

    static ServerDispatcher instance;
    LoggingHelper logger{INFO};
    std::mutex electionStateMutex;
    bool electionOngoing{false};
    bool electedAsPrincipal{false};
};

#endif
