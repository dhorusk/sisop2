#include <thread>
#include <unistd.h>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ClientConnectionManager.hpp"
#include "ClientDispatcher.hpp"

using namespace std;

ClientConnectionManager ClientConnectionManager::instance;

ClientConnectionManager& ClientConnectionManager::GetInstance() {
    // singleton pattern
    return instance;
}

void ClientConnectionManager::LoginToServer(const string& userName, const string& groupName,
        const string& serverAddress, int serverPort) {
	this->userName = userName;
	this->groupName = groupName;

    logger.Info("User \"" + userName + "\" attempting to login");

	// create socket
	socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (socketDescriptor == -1) {
		logger.Error("Error creating the client's socket: " + string(strerror(errno)));
		throw runtime_error("socket(): " + string(strerror(errno)));
	}

	// setup remote address
	sockaddr_in socketAddress{
		.sin_family{AF_INET},
		.sin_port{htons(serverPort)},
		.sin_addr{
			.s_addr{inet_addr(serverAddress.c_str())},
		},
        .sin_zero{0},
	};

	// initiate socket connection
	int connected{connect(socketDescriptor, (sockaddr*) &socketAddress, sizeof(socketAddress))};
	if (connected == -1) {
        logger.Error("Error connecting: " + string(strerror(errno)));
		throw runtime_error("connect(): " + string(strerror(errno)));
	}

	logger.Info("Connected to " + serverAddress);

	auto event{make_unique<LoginAttemptEvent>(userName, groupName, socketDescriptor)};
	Send(event.get());

	auto returnEvent{Receive()};
	auto& dispatcher{ClientDispatcher::GetInstance()};
	dispatcher.Dispatch(returnEvent.get());
}

void ClientConnectionManager::Send(Event* event) {
	return ConnectionManager::Send(socketDescriptor, event);
}

shared_ptr<Event> ClientConnectionManager::Receive() {
	return ConnectionManager::Receive(socketDescriptor);
}

void ClientConnectionManager::StartListeningForServerEvents() {
	new thread(&ClientConnectionManager::ListenForServerEvents, this);
}

void ClientConnectionManager::ListenForServerEvents() {
	auto& dispatcher{ClientDispatcher::GetInstance()};

    while (true) {
        try {
            // read and dispatch event
            auto event{Receive()};
            dispatcher.Dispatch(event.get());
        }
        catch (exception& e) {
            logger.Error("An error occurred: " + string(e.what()));
    		close(socketDescriptor);
            throw e;
        }
    }
}

void ClientConnectionManager::Logout() {
	auto event{make_unique<LogoutEvent>(userName, groupName, socketDescriptor)};
	auto& dispatcher{ClientDispatcher::GetInstance()};
	dispatcher.Dispatch(event.get());

    close(socketDescriptor);
}

void ClientConnectionManager::StartSendingHeartBeat() {
	new thread(&ClientConnectionManager::SendHeartBeats, this);
}

void ClientConnectionManager::SendHeartBeats() {
	while (true) {
		auto event{make_unique<KeepAliveEvent>(socketDescriptor)};
		auto& dispatcher{ClientDispatcher::GetInstance()};
		dispatcher.Dispatch(event.get());
		sleep(10);
	}
}
