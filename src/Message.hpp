#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <string>

class Message {
  public:
    Message(
        const std::string& timestamp,
        const std::string& userName,
        const std::string& messageContent);
    const std::string& GetAuthor();
    const std::string& GetContent();
    const std::string& GetTimestamp();

  private:
    std::string authorUserName;
    std::string messageContent;
    std::string timestamp;
};

#endif
