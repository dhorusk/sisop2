#include <iostream>
#include "ServerConnectionManager.hpp"
#include "SessionManager.hpp"
#include "LoggingHelper.hpp"

using namespace std;

int main(int argc, char* argv[]) {
	// show help if not all args were given
	// TODO: change this when reading from config files
	if (argc < 3) {
		cout << "Usage:" << endl;
		cout << "\t" << argv[0] << " NUM_MESSAGES SERVER_PORT" << endl;
		cout << endl;
		cout << "Where:" << endl;
		cout << "\t" << "NUM_MESSAGES: Number of messages to send to joining users" << endl;
		cout << "\t" << "SERVER_PORT: Network port for the back-end server" << endl;
		exit(EXIT_SUCCESS);
	}

	LoggingHelper logger(INFO);

	// try to parse maxMessagesToSend
	int maxMessagesToSend{0};
	try {
		maxMessagesToSend = stoul(argv[1]);
		logger.Info("NUM_MESSAGES set to " + to_string(maxMessagesToSend));
	}
	catch (...) {
		logger.Error("Invalid value for NUM_MESSAGES");
		exit(EXIT_FAILURE);
	}

	// try to parse serverPort
	int serverPort{0};
	if (argc >= 3) {
		try {
			serverPort = stoi(argv[2]);
			logger.Info("SERVER_PORT set to " + to_string(serverPort));
		}
		catch (...) {
			logger.Error("Invalid value for SERVER_PORT");
			exit(EXIT_FAILURE);
		}
	}

	auto& connectionManager{ServerConnectionManager::GetInstance()};
	connectionManager.Init();
	connectionManager.SetServerAddress("127.0.0.1");
	connectionManager.SetServerPort(serverPort);
	connectionManager.SetMaxMessagesToSend(maxMessagesToSend);
	connectionManager.StartWaitingForConnections(serverPort);
	connectionManager.StartElection();

	auto& sessionManager{SessionManager::GetInstance()};
	sessionManager.StartCheckingSessions();

	// keep the threads alive
	while (true) {
	}

	return 0;
}
