#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <exception>
#include "MediatorConnectionManager.hpp"
#include "MediatorDispatcher.hpp"

using namespace std;

MediatorConnectionManager MediatorConnectionManager::instance;

MediatorConnectionManager& MediatorConnectionManager::GetInstance() {
    // singleton pattern
    return instance;
}

void MediatorConnectionManager::WaitForConnections(int mediatorPort) {
    // create the server's socket
    int serverSocket{socket(AF_INET, SOCK_STREAM, 0)};
	if (serverSocket == -1) {
		logger.Error("Error creating the front-end server's socket: " + string(strerror(errno)));
        throw runtime_error("socket(): " + string(strerror(errno)));
	}

	// setup remote address
	sockaddr_in serverSocketAddress{
		.sin_family{AF_INET},
		.sin_port{htons(mediatorPort)},
		.sin_addr{
            .s_addr{INADDR_ANY},
        },
        .sin_zero{0},
	};

    // bind socket
	int bound{bind(serverSocket, (sockaddr *) &serverSocketAddress, sizeof(serverSocketAddress))};
	if (bound == -1) {
        logger.Error("Error on binding: " + string(strerror(errno)));
        throw runtime_error("bind(): " + string(strerror(errno)));
	}

    // listen to socket
    int listened{listen(serverSocket, MAX_SIMULTANEOUS_SOCKET_CLIENTS)};
	if (listened == -1) {
        logger.Error("Error on listening: " + string(strerror(errno)));
        throw runtime_error("listen(): " + string(strerror(errno)));
	}

    logger.Info("Front-end server listening on port " + to_string(mediatorPort));

    // process incoming connections
    while (true) {
        sockaddr_in clientSocketAddress{0};
        socklen_t clientSocketAddressLength{0};
        int clientSocket{accept(serverSocket, (sockaddr *) &clientSocketAddress, &clientSocketAddressLength)};
        if (clientSocket == -1) {
            logger.Error("Error on accepting client: " + string(strerror(errno)));
            continue;
        }

        auto clientAddress{string(inet_ntoa(clientSocketAddress.sin_addr))};
        auto clientPort{ntohs(clientSocketAddress.sin_port)};
        logger.Info("Established connection with client " + clientAddress + ":" + to_string(clientPort)
            + " on socket descriptor " + to_string(clientSocket));

        // create mutex for writing to this socket
        socketDescriptorMutexes[clientSocket];

        // create thread for reading from this socket
        new thread(&MediatorConnectionManager::ListenForClientEvents, this, clientSocket);
    }
}

void MediatorConnectionManager::SendToPrincipal(Event* event) {
    // lock this socket descriptor to prevent simultaneous writes
    scoped_lock lock(principalSocketDescriptorMutex, socketDescriptorMutexes[principalSocketDescriptor]);

    // check that we know a principal before attempting to send anything
    // if we don't, ignore the message
    if (principalSocketDescriptor == -1) {
        logger.Info("No principal yet");
        return;
    }

	ConnectionManager::Send(principalSocketDescriptor, event);
}

void MediatorConnectionManager::Send(int socketDescriptor, Event* event) {
    // lock this socket descriptor to prevent simultaneous writes
    scoped_lock lock(socketDescriptorMutexes[socketDescriptor]);

	ConnectionManager::Send(socketDescriptor, event);
}

void MediatorConnectionManager::ListenForClientEvents(int socketDescriptor) {
    auto& dispatcher{MediatorDispatcher::GetInstance()};

    while (true) {
        try {
            // read and dispatch event
            auto event{Receive(socketDescriptor)};

            // if this is a client, enrich the event with the source identifiers
            if (socketDescriptor != principalSocketDescriptor) {
                event->SetClientSocketDescriptor(socketDescriptor);
                event->SetMediatorAddress(mediatorAddress);
                event->SetMediatorPort(mediatorPort);
            }

            dispatcher.Dispatch(event.get());
        }
        catch (exception& e) {
            logger.Error("An error occurred: " + string(e.what()));
            break;
        }
    }
}

void MediatorConnectionManager::SetPrincipalSocketDescriptor(int socketDescriptor) {
    scoped_lock lock(principalSocketDescriptorMutex);

    this->principalSocketDescriptor = socketDescriptor;
}

const string& MediatorConnectionManager::GetMediatorAddress() {
    return mediatorAddress;
}

void MediatorConnectionManager::SetMediatorAddress(const string& mediatorAddress) {
    this->mediatorAddress = mediatorAddress;
}

int MediatorConnectionManager::GetMediatorPort() {
    return mediatorPort;
}

void MediatorConnectionManager::SetMediatorPort(int mediatorPort) {
    this->mediatorPort = mediatorPort;
}
