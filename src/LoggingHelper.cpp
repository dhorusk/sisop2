#include <fstream>
#include "LoggingHelper.hpp"

using namespace std;

LoggingHelper::LoggingHelper(LoggingLevel level) {
	this->level = level;

	logToFile = false;
}

LoggingHelper::LoggingHelper(LoggingLevel level, const string& fileName) {
	this->level = level;

	logToFile = true;
	logFileName = fileName;
}

void LoggingHelper::Debug(const string& message) {
	if (level < DEBUG) {
		return;
	}

	Log(clog, "DEBUG: " + message);
}

void LoggingHelper::Info(const string& message) {
	if (level < INFO) {
		return;
	}

	Log(clog, "INFO: " + message);
}

void LoggingHelper::Warning(const string& message) {
	if (level < WARN) {
		return;
	}

	Log(cerr, "WARNING: " + message);
}

void LoggingHelper::Error(const string& message) {
	if (level < ERROR) {
		return;
	}

	Log(cerr, "ERROR: " + message);
}

void LoggingHelper::Log(ostream& stream, const string& message) {
	if (logToFile) {
		LogToFile(message);
	}
	else {
		stream << message << endl;
	}
}

void LoggingHelper::LogToFile(const string& message) {
	ofstream file(logFileName, ios::app);
	file << message << endl;
}
