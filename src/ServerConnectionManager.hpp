#ifndef SERVER_CONNECTION_MANAGER_HPP
#define SERVER_CONNECTION_MANAGER_HPP

#include <memory>
#include <thread>
#include <mutex>
#include <string>
#include <unordered_map>
#include "ConnectionManager.hpp"
#include "LoggingHelper.hpp"

class ServerConnectionManager : public ConnectionManager {
  public:
    static const int DEFAULT_SERVER_PORT{31337};
    static const int MAX_SIMULTANEOUS_SOCKET_CLIENTS{5};

    static ServerConnectionManager& GetInstance();
    void Init();
    const std::string& GetServerAddress();
    void SetServerAddress(const std::string& sserverAddress);
    int GetServerPort();
    void SetServerPort(int serverPort);
    unsigned long GetMaxMessagesToSend();
    void SetMaxMessagesToSend(unsigned long maxMessagesToSend);
    void StartWaitingForConnections(int serverPort);
    int ConnectToRingPeer();
    void StartElection();
    void ConnectToMediatorsAsPrincipal();
    int GetMediatorSocketDescriptorByAddressAndPort(
        const std::string& mediatorAddress,
        int mediatorPort);
    void StartMonitoringPrincipal(const std::string& principalAddress, int principalPort);
    void MonitorPrincipal(const std::string& principalAddress, int principalPort);
    void Send(int socketDescriptor, Event* event);
    void ForwardToPeers(Event* event);

  private:
    ServerConnectionManager() {};
    ServerConnectionManager(ServerConnectionManager const &) = delete;
    ServerConnectionManager &operator=(ServerConnectionManager const &) = delete;
    void WaitForConnections(int serverPort);
    int Connect(const std::string& serverAddress, int serverPort);
    void ListenForEvents(int socketDescriptor);

    static ServerConnectionManager instance;
    std::unordered_map<int, std::mutex> socketDescriptorMutexes;
    LoggingHelper logger{INFO};
    unsigned long maxMessagesToSend{0};
    std::string serverAddress;
    int serverPort{-1};

    struct PairStringIntHash {
        std::size_t operator()(std::pair<std::string, int> const& p) const noexcept {
            return std::hash<std::string>{}(p.first + std::to_string(p.second));
        }
    };

    std::unordered_map<std::pair<std::string, int>, int, PairStringIntHash> frontEndServers;
    std::unordered_map<std::pair<std::string, int>, int, PairStringIntHash> backEndServers;
};

#endif
