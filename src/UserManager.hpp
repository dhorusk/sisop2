#ifndef USER_MANAGER_HPP
#define USER_MANAGER_HPP

#include <string>
#include <unordered_set>
#include "LoggingHelper.hpp"

class UserManager {
  public:
    static UserManager& GetInstance();
    const std::unordered_set<std::string>& GetAllUsers();
    void AddUser(const std::string& userName);
    void RemoveUser(const std::string& userName);
    bool IsUserNameValid(const std::string& userName);

  private:
    UserManager() {};
    UserManager(UserManager const &) = delete;
    UserManager &operator=(UserManager const &) = delete;

    static UserManager instance;
    std::unordered_set<std::string> users;
    LoggingHelper logger{INFO};
};

#endif
