#include "ClientDispatcher.hpp"
#include "ClientConnectionManager.hpp"
#include "UIManager.hpp"
#include "Event.hpp"

using namespace std;

ClientDispatcher ClientDispatcher::instance;

ClientDispatcher& ClientDispatcher::GetInstance() {
    // singleton pattern
    return instance;
}

void ClientDispatcher::Dispatch(Event* event) {
    logger.Info("Dispatching event with type " + to_string(event->GetType()));
    
    switch(event->GetType()) {
        case KeepAlive:
            return Dispatch(static_cast<KeepAliveEvent*>(event));
        case LoginAttempt:
            return Dispatch(static_cast<LoginAttemptEvent*>(event));
        case LoginFailed:
            return Dispatch(static_cast<LoginFailedEvent*>(event));
        case LoginSucceeded:
            return Dispatch(static_cast<LoginSucceededEvent*>(event));
        case Logout:
            return Dispatch(static_cast<LogoutEvent*>(event));
        case UserSentMessage:
            return Dispatch(static_cast<UserSentMessageEvent*>(event));
        case GroupReceivedMessage:
            return Dispatch(static_cast<GroupReceivedMessageEvent*>(event));
        case UserJoinedGroup:
            return Dispatch(static_cast<UserJoinedGroupEvent*>(event));
        case UserLeftGroup:
            return Dispatch(static_cast<UserLeftGroupEvent*>(event));
        case ElectionStarted:
            return; // client should never receive this event
        case ElectionEnded:
            return Dispatch(static_cast<ElectionEndedEvent*>(event));
    }
}

void ClientDispatcher::Dispatch(KeepAliveEvent* event) {
    logger.Info("Send KeepAliveEvent as heartbeat");
    auto& connectionManager{ClientConnectionManager::GetInstance()};
    connectionManager.Send(event);
}

void ClientDispatcher::Dispatch(LoginFailedEvent* event) {
    logger.Info("Login failed: " + event->GetReason());

    auto& ui{UIManager::GetInstance()};
    ui.~UIManager();

    cout << "Login failed: " + event->GetReason() << endl;
    exit(EXIT_FAILURE);
}

void ClientDispatcher::Dispatch(LoginSucceededEvent* event) {
    logger.Info("Login to group successful");

    auto& connectionManager{ClientConnectionManager::GetInstance()};
    connectionManager.StartListeningForServerEvents();
    connectionManager.StartSendingHeartBeat();
}

void ClientDispatcher::Dispatch(LogoutEvent* event) {
    logger.Info("User just logged out");
    auto& connectionManager{ClientConnectionManager::GetInstance()};
    connectionManager.Send(event);
}

void ClientDispatcher::Dispatch(UserSentMessageEvent* event) {
    logger.Info("User sent message to group");
    auto& connectionManager{ClientConnectionManager::GetInstance()};
    connectionManager.Send(event);
}

void ClientDispatcher::Dispatch(GroupReceivedMessageEvent* event) {
    logger.Info("Group received message from a user");
    auto& ui{UIManager::GetInstance()};
    ui.DisplayMessage(event->GetUserName(), event->GetMessageContent(), event->GetTimestamp());
}

void ClientDispatcher::Dispatch(UserJoinedGroupEvent* event) {
    logger.Info("User " + event->GetUserName() + " joined group " + event->GetGroupName());
    auto& ui{UIManager::GetInstance()};
    ui.DisplayEvent("User " + event->GetUserName() + " joined the group!", event->GetTimestamp());
}

void ClientDispatcher::Dispatch(UserLeftGroupEvent* event) {
    logger.Info("User " + event->GetUserName() + " left group " + event->GetGroupName());
    auto& ui{UIManager::GetInstance()};
    ui.DisplayEvent("User " + event->GetUserName() + " left the group!", event->GetTimestamp());
}
