#ifndef CLIENT_CONNECTION_MANAGER_HPP
#define CLIENT_CONNECTION_MANAGER_HPP

#include <memory>
#include <string>
#include "ConnectionManager.hpp"
#include "LoggingHelper.hpp"

class ClientConnectionManager : public ConnectionManager {
  public:
    static ClientConnectionManager& GetInstance();
    void LoginToServer(const std::string& userName, const std::string& groupName,
        const std::string& serverAddress, int serverPort);
    void Send(Event* event);
    std::shared_ptr<Event> Receive();
    void ListenForServerEvents();
    void StartListeningForServerEvents();
    void Logout();
    void StartSendingHeartBeat();
    void SendHeartBeats();

  private:
    ClientConnectionManager() {};
    ClientConnectionManager(ClientConnectionManager const &) = delete;
    ClientConnectionManager &operator=(ClientConnectionManager const &) = delete;

    static ClientConnectionManager instance;
    int socketDescriptor{-1};
    std::string userName;
    std::string groupName;
    LoggingHelper logger{ERROR};
};

#endif
