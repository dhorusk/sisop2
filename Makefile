CC = g++
CFLAGS = -g -O2 -std=c++17 -Wall -Wno-unknown-pragmas -lpthread -lncurses
SRC_DIR = ./src
OBJ_DIR = ./obj

.PHONY: all clean

all: client mediator server

CLIENT_DEPS += $(OBJ_DIR)/Client.o
CLIENT_DEPS += $(OBJ_DIR)/ClientConnectionManager.o
CLIENT_DEPS += $(OBJ_DIR)/ClientDispatcher.o
CLIENT_DEPS += $(OBJ_DIR)/ConnectionManager.o
CLIENT_DEPS += $(OBJ_DIR)/Event.o
CLIENT_DEPS += $(OBJ_DIR)/LoggingHelper.o
CLIENT_DEPS += $(OBJ_DIR)/UIManager.o
client: $(CLIENT_DEPS)
	$(CC) -o $@ $^ $(CFLAGS)

MEDIATOR_DEPS += $(OBJ_DIR)/Mediator.o
MEDIATOR_DEPS += $(OBJ_DIR)/MediatorConnectionManager.o
MEDIATOR_DEPS += $(OBJ_DIR)/MediatorDispatcher.o
MEDIATOR_DEPS += $(OBJ_DIR)/ConnectionManager.o
MEDIATOR_DEPS += $(OBJ_DIR)/Event.o
MEDIATOR_DEPS += $(OBJ_DIR)/LoggingHelper.o
mediator: $(MEDIATOR_DEPS)
	$(CC) -o $@ $^ $(CFLAGS)

SERVER_DEPS += $(OBJ_DIR)/Server.o
SERVER_DEPS += $(OBJ_DIR)/ServerConnectionManager.o
SERVER_DEPS += $(OBJ_DIR)/ServerDispatcher.o
SERVER_DEPS += $(OBJ_DIR)/ConnectionManager.o
SERVER_DEPS += $(OBJ_DIR)/Event.o
SERVER_DEPS += $(OBJ_DIR)/Group.o
SERVER_DEPS += $(OBJ_DIR)/MessageRepository.o
SERVER_DEPS += $(OBJ_DIR)/GroupManager.o
SERVER_DEPS += $(OBJ_DIR)/LoggingHelper.o
SERVER_DEPS += $(OBJ_DIR)/Message.o
SERVER_DEPS += $(OBJ_DIR)/Session.o
SERVER_DEPS += $(OBJ_DIR)/SessionManager.o
SERVER_DEPS += $(OBJ_DIR)/UserManager.o
server: $(SERVER_DEPS)
	$(CC) -o $@ $^ $(CFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm -rf client server $(OBJ_DIR)/*.o $(SRC_DIR)/*~
